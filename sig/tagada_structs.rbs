class DataClass
  def to_json: (*untyped) -> String

  def self.from_json: (json: Hash[Symbol, untyped]) -> self
end

class Version < DataClass
  attr_reader major: Integer
  attr_reader minor: Integer
  attr_reader patch: Integer
end

class StateNodeUID < DataClass
  attr_reader uid: Integer
end

class FunctionNodeUID < DataClass
  attr_reader uid: Integer
end

class Domain < DataClass
  attr_reader type: String

  def mask: () -> Integer
end

class HalfOpenRange < Domain
  attr_reader type: String
  attr_reader min: Integer
  attr_reader max: Integer

  def mask: () -> Integer
end

class Sparse < Domain
  attr_reader type: String
  attr_reader values: Array[Integer]

  def mask: () -> Integer
end

class Function < DataClass
  attr_reader type: String
end

class Equal < Function
  attr_reader type: String
end

class BitwiseXOR < Function
  attr_reader type: String
end

class SBox < Function
  attr_reader type: String
  attr_reader lookup_table: Array[Integer]
end

class Table < Function
  attr_reader type: String
  attr_reader tuples: Array[Array[Integer]]
  attr_reader is_linear: bool
end

class Command < Function
  attr_reader type: String
  attr_reader executable: String
  attr_reader is_linear: bool
end

class GFMul < Function
  attr_reader type: String
  attr_reader poly: Integer
  attr_reader constant: Integer
end

class GFMatToVecMul < Function
  attr_reader type: String
  attr_reader poly: Integer
  attr_reader m: Array[Array[Integer]]
end

class LFSR < Function
  attr_reader type: String
  attr_reader nb_bits: Integer
  attr_reader poly: Integer
  attr_reader direction: String
end

class Split < Function
  attr_reader type: String
  attr_reader to_words: Array[Array[Integer]]
end

class BitAndWord < DataClass
  attr_reader bit_pos: Integer
  attr_reader word_pos: Integer
end

class Concat < Function
  attr_reader type: String
  attr_reader to_word: Array[BitAndWord]
end

class DifferentialFunction < DataClass
  attr_reader type: String
end

type minus_log2_probability = Integer?

class MinusLog2DDT < DifferentialFunction
  attr_reader type: String
  attr_reader probabilities: Array[Array[minus_log2_probability]]
  attr_reader coefficient: Integer
end

class Linear < DifferentialFunction
  attr_reader type: String
  attr_reader function: Function
end

class FunctionNode < DataClass
  attr_reader domain: Array[Domain]
  attr_reader co_domain: Array[Domain]
  attr_reader function: Function
end

class DifferentialFunctionNode < DataClass
  attr_reader domain: Array[Domain]
  attr_reader co_domain: Array[Domain]
  attr_reader function: DifferentialFunction
end

class Transition < DataClass
  attr_reader inputs: Array[StateNodeUID]
  attr_reader function: FunctionNodeUID
  attr_reader outputs: Array[StateNodeUID]
end

class StateNode < DataClass
  attr_reader domain: Array[Domain]
  attr_reader value: Integer?
end

class DifferentialCipherDag < DataClass
  attr_reader version: Version
  attr_reader states: Array[StateNode]
  attr_reader functions: Array[DifferentialFunctionNode]
  attr_reader transitions: Array[Transition]
  attr_reader plaintext: Array[StateNodeUID]
  attr_reader key: Array[StateNodeUID]
  attr_reader ciphertext: Array[StateNodeUID]
  attr_reader names: Hash[String, StateNodeUID]
  attr_reader blocks: Hash[String, Array[StateNodeUID]]
end

class CipherDag < DataClass
  attr_reader version: Version
  attr_reader states: Array[StateNode]
  attr_reader functions: Array[FunctionNode]
  attr_reader transitions: Array[Transition]
  attr_reader plaintext: Array[StateNodeUID]
  attr_reader key: Array[StateNodeUID]
  attr_reader ciphertext: Array[StateNodeUID]
  attr_reader names: Hash[String, StateNodeUID]
  attr_reader blocks: Hash[String, Array[StateNodeUID]]
end