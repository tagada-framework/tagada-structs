#!/usr/bin/env kscript

import java.lang.ProcessBuilder
import java.nio.file.Files
import java.nio.file.Path

data class Version(val major: Int, val minor: Int, val patch: Int) {
    companion object {
        fun CURRENT(): Version {
            val version = Path.of("version.txt")
            val versionFields = Files.readString(version).trim()
                .split('.')
                .map(String::toInt)
            return Version(versionFields[0], versionFields[1], versionFields[2])
        }
    }
}

data class Configuration(val path: String, val regex: String, val serializer: (Version) -> String)

val CONFIGURATIONS = listOf(
    Configuration("Cargo.toml", "version ?= ?\"([\\d+]\\.[\\d+].[\\d+])\"", ::toToml),
    Configuration("build.gradle.kts", "version ?= ?\"([\\d+]\\.[\\d+].[\\d+])\"", ::toToml),
    Configuration("README.md", "Latest version: `([\\d+]\\.[\\d+].[\\d+])`", ::toREADME),
    Configuration("tagada_structs.gemspec", "  s.version     = \"([\\d+]\\.[\\d+].[\\d+])\"", ::toGemSpec)
)

fun toToml(version: Version) = "version = \"${version.major}.${version.minor}.${version.patch}\""
fun toREADME(version: Version) = "Latest version: `${version.major}.${version.minor}.${version.patch}`"
fun toGemSpec(version: Version) = "  s.version     = \"${version.major}.${version.minor}.${version.patch}\""

fun main() {
    val currentVersion = Version.CURRENT()
    for ((path, regex, serializer) in CONFIGURATIONS) {
        val re = regex.toRegex()
        val lines = mutableListOf<String>()
        for (line in Files.readAllLines(Path.of(path))) {
            if (re.matches(line)) {
                lines += re.replace(line, serializer(currentVersion))
            } else {
                lines += line
            }
        }
        Files.write(Path.of(path), lines)
    }
}

