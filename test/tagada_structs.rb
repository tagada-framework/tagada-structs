require "minitest/autorun"
require_relative "../lib/common"
require_relative "../lib/specification"

def read_text_from(path)
  File.read("src/test/resources/#{path}.json")
end

def read_json_from(path)
  json = JSON.load(File.read("src/test/resources/#{path}.json"))
  if json.is_a? Hash
    json = json.transform_keys(&:to_sym)
  end
  json
end

class TestSerialize < Minitest::Test

  def test_version()
    version = Version.new(major: 1, minor: 2, patch: 3)
    assert_equal read_text_from('version'), version.to_json
  end

  def test_variable()
    variable = Specification::Variable.new(
      domain: HalfOpenRange.new(min: 0, max: 256)
    )
    assert_equal read_text_from("state_node/variable"), variable.to_json
  end

  def test_constant()
    constant = Specification::Constant.new(
      value: 10
    )
    assert_equal read_text_from("state_node/constant"), constant.to_json
  end

  def test_range_domain()
    nibble = HalfOpenRange.new(min: 0, max: 16)
    assert_equal read_text_from("domain/range"), nibble.to_json
  end

  def test_sparse_domain()
    nibble = Sparse.new(values: Array(0...16))
    assert_equal read_text_from("domain/sparse"), nibble.to_json
  end

  def test_function_node()
    byte = HalfOpenRange.new(min: 0, max: 256)
    function_node = Specification::FunctionNode.new(
      domain: [byte],
      co_domain: [byte],
      function: Specification::Linear.new(linear: Specification::Equal.new)
    )
    assert_equal read_text_from("function_node"), function_node.to_json
  end

  def test_equal
    assert_equal read_text_from("function/equal"), Specification::Equal.new.to_json
  end

  def test_bitwise_xor
    assert_equal read_text_from("function/bitwise_xor"), Specification::BitwiseXOR.new.to_json
  end

  def test_sbox
    aes_sbox = Specification::SBox.new(
      lookup_table: [
        0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
        0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
        0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
        0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
        0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
        0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
        0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
        0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
        0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
        0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
        0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
        0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
        0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
        0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
        0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
        0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16,
      ]
    )
    assert_equal read_text_from("function/sbox"), aes_sbox.to_json
  end

  def test_table
    nibble_xor = Specification::Table.new(
      tuples: [
        [0, 0, 0],
        [0, 1, 1],
        [0, 2, 2],
        [0, 3, 3],
        [0, 4, 4],
        [0, 5, 5],
        [0, 6, 6],
        [0, 7, 7],
        [0, 8, 8],
        [0, 9, 9],
        [0, 10, 10],
        [0, 11, 11],
        [0, 12, 12],
        [0, 13, 13],
        [0, 14, 14],
        [0, 15, 15],
        [1, 0, 1],
        [1, 1, 0],
        [1, 2, 3],
        [1, 3, 2],
        [1, 4, 5],
        [1, 5, 4],
        [1, 6, 7],
        [1, 7, 6],
        [1, 8, 9],
        [1, 9, 8],
        [1, 10, 11],
        [1, 11, 10],
        [1, 12, 13],
        [1, 13, 12],
        [1, 14, 15],
        [1, 15, 14],
        [2, 0, 2],
        [2, 1, 3],
        [2, 2, 0],
        [2, 3, 1],
        [2, 4, 6],
        [2, 5, 7],
        [2, 6, 4],
        [2, 7, 5],
        [2, 8, 10],
        [2, 9, 11],
        [2, 10, 8],
        [2, 11, 9],
        [2, 12, 14],
        [2, 13, 15],
        [2, 14, 12],
        [2, 15, 13],
        [3, 0, 3],
        [3, 1, 2],
        [3, 2, 1],
        [3, 3, 0],
        [3, 4, 7],
        [3, 5, 6],
        [3, 6, 5],
        [3, 7, 4],
        [3, 8, 11],
        [3, 9, 10],
        [3, 10, 9],
        [3, 11, 8],
        [3, 12, 15],
        [3, 13, 14],
        [3, 14, 13],
        [3, 15, 12],
        [4, 0, 4],
        [4, 1, 5],
        [4, 2, 6],
        [4, 3, 7],
        [4, 4, 0],
        [4, 5, 1],
        [4, 6, 2],
        [4, 7, 3],
        [4, 8, 12],
        [4, 9, 13],
        [4, 10, 14],
        [4, 11, 15],
        [4, 12, 8],
        [4, 13, 9],
        [4, 14, 10],
        [4, 15, 11],
        [5, 0, 5],
        [5, 1, 4],
        [5, 2, 7],
        [5, 3, 6],
        [5, 4, 1],
        [5, 5, 0],
        [5, 6, 3],
        [5, 7, 2],
        [5, 8, 13],
        [5, 9, 12],
        [5, 10, 15],
        [5, 11, 14],
        [5, 12, 9],
        [5, 13, 8],
        [5, 14, 11],
        [5, 15, 10],
        [6, 0, 6],
        [6, 1, 7],
        [6, 2, 4],
        [6, 3, 5],
        [6, 4, 2],
        [6, 5, 3],
        [6, 6, 0],
        [6, 7, 1],
        [6, 8, 14],
        [6, 9, 15],
        [6, 10, 12],
        [6, 11, 13],
        [6, 12, 10],
        [6, 13, 11],
        [6, 14, 8],
        [6, 15, 9],
        [7, 0, 7],
        [7, 1, 6],
        [7, 2, 5],
        [7, 3, 4],
        [7, 4, 3],
        [7, 5, 2],
        [7, 6, 1],
        [7, 7, 0],
        [7, 8, 15],
        [7, 9, 14],
        [7, 10, 13],
        [7, 11, 12],
        [7, 12, 11],
        [7, 13, 10],
        [7, 14, 9],
        [7, 15, 8],
        [8, 0, 8],
        [8, 1, 9],
        [8, 2, 10],
        [8, 3, 11],
        [8, 4, 12],
        [8, 5, 13],
        [8, 6, 14],
        [8, 7, 15],
        [8, 8, 0],
        [8, 9, 1],
        [8, 10, 2],
        [8, 11, 3],
        [8, 12, 4],
        [8, 13, 5],
        [8, 14, 6],
        [8, 15, 7],
        [9, 0, 9],
        [9, 1, 8],
        [9, 2, 11],
        [9, 3, 10],
        [9, 4, 13],
        [9, 5, 12],
        [9, 6, 15],
        [9, 7, 14],
        [9, 8, 1],
        [9, 9, 0],
        [9, 10, 3],
        [9, 11, 2],
        [9, 12, 5],
        [9, 13, 4],
        [9, 14, 7],
        [9, 15, 6],
        [10, 0, 10],
        [10, 1, 11],
        [10, 2, 8],
        [10, 3, 9],
        [10, 4, 14],
        [10, 5, 15],
        [10, 6, 12],
        [10, 7, 13],
        [10, 8, 2],
        [10, 9, 3],
        [10, 10, 0],
        [10, 11, 1],
        [10, 12, 6],
        [10, 13, 7],
        [10, 14, 4],
        [10, 15, 5],
        [11, 0, 11],
        [11, 1, 10],
        [11, 2, 9],
        [11, 3, 8],
        [11, 4, 15],
        [11, 5, 14],
        [11, 6, 13],
        [11, 7, 12],
        [11, 8, 3],
        [11, 9, 2],
        [11, 10, 1],
        [11, 11, 0],
        [11, 12, 7],
        [11, 13, 6],
        [11, 14, 5],
        [11, 15, 4],
        [12, 0, 12],
        [12, 1, 13],
        [12, 2, 14],
        [12, 3, 15],
        [12, 4, 8],
        [12, 5, 9],
        [12, 6, 10],
        [12, 7, 11],
        [12, 8, 4],
        [12, 9, 5],
        [12, 10, 6],
        [12, 11, 7],
        [12, 12, 0],
        [12, 13, 1],
        [12, 14, 2],
        [12, 15, 3],
        [13, 0, 13],
        [13, 1, 12],
        [13, 2, 15],
        [13, 3, 14],
        [13, 4, 9],
        [13, 5, 8],
        [13, 6, 11],
        [13, 7, 10],
        [13, 8, 5],
        [13, 9, 4],
        [13, 10, 7],
        [13, 11, 6],
        [13, 12, 1],
        [13, 13, 0],
        [13, 14, 3],
        [13, 15, 2],
        [14, 0, 14],
        [14, 1, 15],
        [14, 2, 12],
        [14, 3, 13],
        [14, 4, 10],
        [14, 5, 11],
        [14, 6, 8],
        [14, 7, 9],
        [14, 8, 6],
        [14, 9, 7],
        [14, 10, 4],
        [14, 11, 5],
        [14, 12, 2],
        [14, 13, 3],
        [14, 14, 0],
        [14, 15, 1],
        [15, 0, 15],
        [15, 1, 14],
        [15, 2, 13],
        [15, 3, 12],
        [15, 4, 11],
        [15, 5, 10],
        [15, 6, 9],
        [15, 7, 8],
        [15, 8, 7],
        [15, 9, 6],
        [15, 10, 5],
        [15, 11, 4],
        [15, 12, 3],
        [15, 13, 2],
        [15, 14, 1],
        [15, 15, 0],
      ]
    )
    assert_equal read_text_from("function/table"), nibble_xor.to_json
  end

  def test_command
    user_command = Specification::Command.new(
      executable: "user_command"
    )
    assert_equal read_text_from("function/command"), user_command.to_json
  end

  def test_gf_mul
    aes_poly = Specification::GFMul.new(poly: 0x11B, constant: 0x53)
    assert_equal read_text_from("function/gfmul"), aes_poly.to_json
  end

  def test_gf_mat_to_vec_mul
    aes_mat_mul = Specification::GFMatToVecMul.new(
      poly: 0x11B,
      m: [
        [2, 3, 1, 1],
        [1, 2, 3, 1],
        [1, 1, 2, 3],
        [3, 1, 1, 2]
      ]
    )
    assert_equal read_text_from("function/gfmattovecmul"), aes_mat_mul.to_json
  end

  def test_lfsr
    lfsr = Specification::LFSR.new(nb_bits: 8, poly: 5, direction: "ShiftLeft")
    assert_equal read_text_from('function/lfsr'), lfsr.to_json
  end

  def test_split
    split = Specification::Split.new(
      to_words: [
        [7, 6, 5, 4],
        [3, 2, 1, 0]
      ]
    )
    assert_equal read_text_from("function/split"), split.to_json
  end

  def test_concat
    concat = Specification::Concat.new(
      to_word: [
        Specification::BitAndWord.new(bit_pos: 1, word_pos: 1),
        Specification::BitAndWord.new(bit_pos: 0, word_pos: 1),
        Specification::BitAndWord.new(bit_pos: 1, word_pos: 0),
        Specification::BitAndWord.new(bit_pos: 0, word_pos: 0),
      ]
    )
    assert_equal read_text_from("function/concat"), concat.to_json
  end

  def test_shift
      shift = Specification::Shift.new(
        nb_bits: 8,
        bit_count: 3,
        direction: "ShiftRight"
      )
      assert_equal read_text_from("function/shift"), shift.to_json
  end

  def test_circular_shift
      circular_shift = Specification::CircularShift.new(
        nb_bits: 8,
        bit_count: 6,
        direction: "ShiftLeft"
      )
      assert_equal read_text_from("function/circular_shift"), circular_shift.to_json
  end

  def test_and_const
      and_const = Specification::AndConst.new(
        constant: 83
      )
      assert_equal read_text_from("function/and_const"), and_const.to_json
  end

  def test_or_const
      or_const = Specification::OrConst.new(
        constant: 83
      )
      assert_equal read_text_from("function/or_const"), or_const.to_json
  end

  def test_bit_and_word
    bit_and_word = Specification::BitAndWord.new(
      bit_pos: 1,
      word_pos: 2
    )
    assert_equal read_text_from("bit_and_word"), bit_and_word.to_json
  end

  def test_shiftleft
    assert_equal(read_text_from("shiftleft"),"ShiftLeft".to_json);
  end

  def test_shiftright
    assert_equal(read_text_from("shiftright"), "ShiftRight".to_json);
  end

  def test_transition
    transition = Specification::Transition.new(
      inputs: [Specification::StateNodeUID.new(uid: 0)],
      function: Specification::FunctionNodeUID.new(uid: 0),
      outputs: [Specification::StateNodeUID.new(uid: 1)]
    )
    assert_equal(read_text_from("transition"), transition.to_json)
  end

  def test_function_node_uid
    function_node_uid = Specification::FunctionNodeUID.new(uid: 10);
    assert_equal(read_text_from("function_node_uid"), function_node_uid.to_json)
  end

  def test_state_node_uid
    state_node_uid = Specification::StateNodeUID.new(uid: 4)
    assert_equal(read_text_from("state_node_uid"), state_node_uid.to_json)
  end
end

class TestDeserialize < MiniTest::Test

  def test_version
    version = Version.new(major: 1, minor: 2, patch: 3)
    assert_equal version, Version.from_json(read_json_from('version'))
  end

  def test_variable
    variable = Specification::Variable.new(
      domain: HalfOpenRange.new(min: 0, max: 256),
    )
    assert_equal variable, Specification::StateNode.from_json(read_json_from('state_node/variable'))
  end

  def test_constant
    constant = Specification::Constant.new(
      value: 10
    )
    assert_equal constant, Specification::StateNode.from_json(read_json_from('state_node/constant'))
  end

  def test_range_domain
    nibble = HalfOpenRange.new(min: 0, max: 16)
    assert_equal nibble, Domain.from_json(read_json_from('domain/range'))
  end

  def test_sparse_domain
    nibble = Sparse.new(values: Array(0...16))
    assert_equal nibble, Domain.from_json(read_json_from('domain/sparse'))
  end

  def test_function_node
    byte = HalfOpenRange.new(min: 0, max: 256)
    function_node = Specification::FunctionNode.new(
      domain: [byte],
      co_domain: [byte],
      function: Specification::Linear.new(linear: Specification::Equal.new)
    )
    assert_equal function_node, Specification::FunctionNode.from_json(read_json_from("function_node"))
  end

  def test_equal
    assert_equal Specification::Equal.new, Specification::LinearFunction.from_json(read_json_from('function/equal'))
  end

  def test_bitwise_xor
    assert_equal Specification::BitwiseXOR.new, Specification::LinearFunction.from_json(read_json_from('function/bitwise_xor'))
  end

  def test_sbox
    aes_sbox = Specification::SBox.new(
      lookup_table: [
        0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
        0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
        0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
        0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
        0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
        0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
        0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
        0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
        0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
        0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
        0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
        0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
        0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
        0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
        0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
        0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16,
      ]
    )
    assert_equal aes_sbox, Specification::Function.from_json(read_json_from('function/sbox'))
  end

  def test_table
    nibble_xor = Specification::Table.new(
      tuples: [
        [0, 0, 0],
        [0, 1, 1],
        [0, 2, 2],
        [0, 3, 3],
        [0, 4, 4],
        [0, 5, 5],
        [0, 6, 6],
        [0, 7, 7],
        [0, 8, 8],
        [0, 9, 9],
        [0, 10, 10],
        [0, 11, 11],
        [0, 12, 12],
        [0, 13, 13],
        [0, 14, 14],
        [0, 15, 15],
        [1, 0, 1],
        [1, 1, 0],
        [1, 2, 3],
        [1, 3, 2],
        [1, 4, 5],
        [1, 5, 4],
        [1, 6, 7],
        [1, 7, 6],
        [1, 8, 9],
        [1, 9, 8],
        [1, 10, 11],
        [1, 11, 10],
        [1, 12, 13],
        [1, 13, 12],
        [1, 14, 15],
        [1, 15, 14],
        [2, 0, 2],
        [2, 1, 3],
        [2, 2, 0],
        [2, 3, 1],
        [2, 4, 6],
        [2, 5, 7],
        [2, 6, 4],
        [2, 7, 5],
        [2, 8, 10],
        [2, 9, 11],
        [2, 10, 8],
        [2, 11, 9],
        [2, 12, 14],
        [2, 13, 15],
        [2, 14, 12],
        [2, 15, 13],
        [3, 0, 3],
        [3, 1, 2],
        [3, 2, 1],
        [3, 3, 0],
        [3, 4, 7],
        [3, 5, 6],
        [3, 6, 5],
        [3, 7, 4],
        [3, 8, 11],
        [3, 9, 10],
        [3, 10, 9],
        [3, 11, 8],
        [3, 12, 15],
        [3, 13, 14],
        [3, 14, 13],
        [3, 15, 12],
        [4, 0, 4],
        [4, 1, 5],
        [4, 2, 6],
        [4, 3, 7],
        [4, 4, 0],
        [4, 5, 1],
        [4, 6, 2],
        [4, 7, 3],
        [4, 8, 12],
        [4, 9, 13],
        [4, 10, 14],
        [4, 11, 15],
        [4, 12, 8],
        [4, 13, 9],
        [4, 14, 10],
        [4, 15, 11],
        [5, 0, 5],
        [5, 1, 4],
        [5, 2, 7],
        [5, 3, 6],
        [5, 4, 1],
        [5, 5, 0],
        [5, 6, 3],
        [5, 7, 2],
        [5, 8, 13],
        [5, 9, 12],
        [5, 10, 15],
        [5, 11, 14],
        [5, 12, 9],
        [5, 13, 8],
        [5, 14, 11],
        [5, 15, 10],
        [6, 0, 6],
        [6, 1, 7],
        [6, 2, 4],
        [6, 3, 5],
        [6, 4, 2],
        [6, 5, 3],
        [6, 6, 0],
        [6, 7, 1],
        [6, 8, 14],
        [6, 9, 15],
        [6, 10, 12],
        [6, 11, 13],
        [6, 12, 10],
        [6, 13, 11],
        [6, 14, 8],
        [6, 15, 9],
        [7, 0, 7],
        [7, 1, 6],
        [7, 2, 5],
        [7, 3, 4],
        [7, 4, 3],
        [7, 5, 2],
        [7, 6, 1],
        [7, 7, 0],
        [7, 8, 15],
        [7, 9, 14],
        [7, 10, 13],
        [7, 11, 12],
        [7, 12, 11],
        [7, 13, 10],
        [7, 14, 9],
        [7, 15, 8],
        [8, 0, 8],
        [8, 1, 9],
        [8, 2, 10],
        [8, 3, 11],
        [8, 4, 12],
        [8, 5, 13],
        [8, 6, 14],
        [8, 7, 15],
        [8, 8, 0],
        [8, 9, 1],
        [8, 10, 2],
        [8, 11, 3],
        [8, 12, 4],
        [8, 13, 5],
        [8, 14, 6],
        [8, 15, 7],
        [9, 0, 9],
        [9, 1, 8],
        [9, 2, 11],
        [9, 3, 10],
        [9, 4, 13],
        [9, 5, 12],
        [9, 6, 15],
        [9, 7, 14],
        [9, 8, 1],
        [9, 9, 0],
        [9, 10, 3],
        [9, 11, 2],
        [9, 12, 5],
        [9, 13, 4],
        [9, 14, 7],
        [9, 15, 6],
        [10, 0, 10],
        [10, 1, 11],
        [10, 2, 8],
        [10, 3, 9],
        [10, 4, 14],
        [10, 5, 15],
        [10, 6, 12],
        [10, 7, 13],
        [10, 8, 2],
        [10, 9, 3],
        [10, 10, 0],
        [10, 11, 1],
        [10, 12, 6],
        [10, 13, 7],
        [10, 14, 4],
        [10, 15, 5],
        [11, 0, 11],
        [11, 1, 10],
        [11, 2, 9],
        [11, 3, 8],
        [11, 4, 15],
        [11, 5, 14],
        [11, 6, 13],
        [11, 7, 12],
        [11, 8, 3],
        [11, 9, 2],
        [11, 10, 1],
        [11, 11, 0],
        [11, 12, 7],
        [11, 13, 6],
        [11, 14, 5],
        [11, 15, 4],
        [12, 0, 12],
        [12, 1, 13],
        [12, 2, 14],
        [12, 3, 15],
        [12, 4, 8],
        [12, 5, 9],
        [12, 6, 10],
        [12, 7, 11],
        [12, 8, 4],
        [12, 9, 5],
        [12, 10, 6],
        [12, 11, 7],
        [12, 12, 0],
        [12, 13, 1],
        [12, 14, 2],
        [12, 15, 3],
        [13, 0, 13],
        [13, 1, 12],
        [13, 2, 15],
        [13, 3, 14],
        [13, 4, 9],
        [13, 5, 8],
        [13, 6, 11],
        [13, 7, 10],
        [13, 8, 5],
        [13, 9, 4],
        [13, 10, 7],
        [13, 11, 6],
        [13, 12, 1],
        [13, 13, 0],
        [13, 14, 3],
        [13, 15, 2],
        [14, 0, 14],
        [14, 1, 15],
        [14, 2, 12],
        [14, 3, 13],
        [14, 4, 10],
        [14, 5, 11],
        [14, 6, 8],
        [14, 7, 9],
        [14, 8, 6],
        [14, 9, 7],
        [14, 10, 4],
        [14, 11, 5],
        [14, 12, 2],
        [14, 13, 3],
        [14, 14, 0],
        [14, 15, 1],
        [15, 0, 15],
        [15, 1, 14],
        [15, 2, 13],
        [15, 3, 12],
        [15, 4, 11],
        [15, 5, 10],
        [15, 6, 9],
        [15, 7, 8],
        [15, 8, 7],
        [15, 9, 6],
        [15, 10, 5],
        [15, 11, 4],
        [15, 12, 3],
        [15, 13, 2],
        [15, 14, 1],
        [15, 15, 0],
      ]
    )
    assert_equal nibble_xor, Specification::LinearFunction.from_json(read_json_from('function/table'))
  end

  def test_command
    user_command = Specification::Command.new(
      executable: "user_command"
    )
    assert_equal user_command, Specification::LinearFunction.from_json(read_json_from('function/command'))
  end

  def test_gf_mul
    aes_poly = Specification::GFMul.new(poly: 0x11B, constant: 0x53)
    assert_equal aes_poly, Specification::LinearFunction.from_json(read_json_from('function/gfmul'))
  end

  def test_gf_mat_to_vec_mul
    aes_mat_mul = Specification::GFMatToVecMul.new(
      poly: 0x11B,
      m: [
        [2, 3, 1, 1],
        [1, 2, 3, 1],
        [1, 1, 2, 3],
        [3, 1, 1, 2]
      ]
    )
    assert_equal aes_mat_mul, Specification::LinearFunction.from_json(read_json_from('function/gfmattovecmul'))
  end

  def test_lfsr
    lfsr = Specification::LFSR.new(nb_bits: 8, poly: 5, direction: "ShiftLeft")
    assert_equal lfsr, Specification::LinearFunction.from_json(read_json_from('function/lfsr'))
  end

  def test_split
    split = Specification::Split.new(
      to_words: [
        [7, 6, 5, 4],
        [3, 2, 1, 0]
      ]
    )
    assert_equal split, Specification::LinearFunction.from_json(read_json_from('function/split'))
  end

  def test_concat
    concat = Specification::Concat.new(
      to_word: [
        Specification::BitAndWord.new(bit_pos: 1, word_pos: 1),
        Specification::BitAndWord.new(bit_pos: 0, word_pos: 1),
        Specification::BitAndWord.new(bit_pos: 1, word_pos: 0),
        Specification::BitAndWord.new(bit_pos: 0, word_pos: 0),
      ]
    )
    assert_equal concat, Specification::LinearFunction.from_json(read_json_from('function/concat'))
  end

  def test_shift
    shift = Specification::Shift.new(
      nb_bits: 8,
      bit_count: 3,
      direction: "ShiftRight"
    )
    assert_equal shift, Specification::LinearFunction.from_json(read_json_from('function/shift'))
  end

  def test_circular_shift
      circular_shift = Specification::CircularShift.new(
        nb_bits: 8,
        bit_count: 6,
        direction: "ShiftLeft"
      )
      assert_equal circular_shift, Specification::LinearFunction.from_json(read_json_from('function/circular_shift'))
  end

  def test_and_const
      and_const = Specification::AndConst.new(
        constant: 83
      )
      assert_equal and_const, Specification::LinearFunction.from_json(read_json_from('function/and_const'))
  end


  def test_or_const
      or_const = Specification::OrConst.new(
        constant: 83
      )
      assert_equal or_const, Specification::LinearFunction.from_json(read_json_from('function/or_const'))
  end


  def test_bit_and_word
    bit_and_word = Specification::BitAndWord.new(
      bit_pos: 1,
      word_pos: 2
    )
    assert_equal bit_and_word, Specification::BitAndWord.from_json(read_json_from("bit_and_word"))
  end

  def test_shiftleft
    assert_equal "ShiftLeft", read_json_from('shiftleft');
  end

  def test_shiftright
    assert_equal "ShiftRight", read_json_from("shiftright");
  end

  def test_transition
    transition = Specification::Transition.new(
      inputs: [Specification::StateNodeUID.new(uid: 0)],
      function: Specification::FunctionNodeUID.new(uid: 0),
      outputs: [Specification::StateNodeUID.new(uid: 1)]
    )
    assert_equal transition, Specification::Transition.from_json(read_json_from('transition'))
  end

  def test_function_node_uid
    function_node_uid = Specification::FunctionNodeUID.new(uid: 10);
    assert_equal function_node_uid, Specification::FunctionNodeUID.from_json(read_json_from('function_node_uid'))
  end

  def test_state_node_uid
    state_node_uid = Specification::StateNodeUID.new(uid: 4)
    assert_equal state_node_uid, Specification::StateNodeUID.from_json(read_json_from('state_node_uid'))
  end

end