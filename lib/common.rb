# frozen_string_literal: true

require 'dry-struct'
require 'json'

module Types
  include Dry.Types()
end

class String
  def substring_after_last(delimiter)
    pos = self.rindex(delimiter)
    if pos == nil
      self
    else
      self[pos + delimiter.size..]
    end
  end
end

CUSTOM_TYPES = {
  "Range" => "HalfOpenRange"
}

def convert(json, cls)
  if cls.name == "Array"
    json.map { |it| convert(it, cls.member) }
  elsif %w[Integer Float String NilClass TrueClass FalseClass Symbol].include? cls.name
    json
  elsif cls.is_a?(Class) and cls <= DataClass
    if json.class == Hash
      json = json.transform_keys(&:to_sym)
    end
    cls.from_json(json)
  else
    types = unpack_types(cls)
    for type in types
      begin
        return convert(json, type)
      rescue
      end
    end
    raise "Unmanaged nested object #{json} for #{cls.name}"
  end
end

def unpack_types(attribute_type)
  types = []
  queue = []
  queue.push attribute_type
  while queue.size > 0
    type = queue.shift
    if type.is_a? Dry::Types::Constrained or type.is_a? Dry::Types::Default
      queue.push type.type
    elsif type.is_a? Dry::Types::Sum
      queue.push type.left
      queue.push type.right
    else
      types.push type
    end
  end
  types
end

class DataClass < Dry::Struct
  def to_json(*options)
    hash = {}
    self.class.schema.keys.each do |key|
      attribute = attributes[key.name]
      if attribute != nil
        hash[key.name] = attribute
      end
    end
    # attributes = self.instance_variable_get(:@attributes)
    # attributes.each { |key, value|
    #  hash[key] = value
    #}
    hash.to_json(*options)
  end

  def self.from_json(json)
    current_class = self
    if self.schema.key? :type
      sub_class_name = CUSTOM_TYPES.fetch(json[:type], json[:type])
      for descendant in self.descendants
        puts descendant.to_s
        if descendant.to_s.end_with? sub_class_name
          current_class = descendant
          # break
        end
      end
      if current_class == self
        raise "#{json[:type]} seems to be not a valid child class for #{self}"
      end
    end
    children = {}
    current_class.schema.keys.each do |attribute|
      if attribute.name != :type
        attribute_type = attribute.type
        types = unpack_types(attribute_type)
        raise "at least one type should be extracted" unless types.size > 0
        used_type = true
        for type in types
          begin
            used_type = true
            children[attribute.name] = convert(json[attribute.name], type)
            break
          rescue
            used_type = false
          end
        end
        unless used_type
          raise "none of #{types} can be used to convert #{json[attribute.name]}"
        end
      end
    end
    return current_class.new(children)
  end
end

class SealedClass < DataClass
  def self.sub_classes(*classes)
    attribute :type, Types::Strict::String.enum(*classes)
  end

  def self.sealed!(type = self.to_s.substring_after_last("::"))
    f_type = type.freeze
    attribute :type, Types::Strict::String.default(f_type).enum(f_type)
    begin
      unless ancestors[1].schema.type.options[:keys][0].values.include? f_type
        raise "#{self} is not declared as a valid child for the class #{self.ancestors[1]}"
      end
    rescue
      raise "#{self} is not declared as a valid child for the class #{self.ancestors[1]}"
    end
  end
end

class Version < DataClass
  attribute :major, Types::Integer
  attribute :minor, Types::Integer
  attribute :patch, Types::Integer

  def to_json(*options)
    [major, minor, patch].to_json(*options)
  end

  def self.from_json(json)
    Version.new(major: json[0], minor: json[1], patch: json[2])
  end
end

class Domain < SealedClass
  sub_classes "Range", "Sparse"

  def mask
    raise "not implemented for #{self.class}"
  end
end

class HalfOpenRange < Domain
  sealed!("Range")
  attribute :min, Types::Strict::Integer
  attribute :max, Types::Strict::Integer

  def mask
    mask = 1
    while mask < (max - 1)
      mask <<= 1
      mask += 1
    end
    mask
  end
end

class Sparse < Domain
  sealed!
  attribute :values, Types::Strict::Array.of(Types::Integer)

  def to_json(*options)
    {
      :type => type,
      :values => values.to_a.sort
    }.to_json(*options)
  end

  def mask
    if values.size == 0
      return nil
    end
    max = values[0]
    for i in 1...values.size
      if values[i] > max
        max = values[i]
      end
    end
    mask = 1
    while mask < max
      mask <<= 1
      mask += 1
    end
    mask
  end
end