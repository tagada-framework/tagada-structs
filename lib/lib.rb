# frozen_string_literal: true

require_relative 'common'
require_relative 'specification'
require_relative 'differential'
require_relative 'truncated_differential'

module Lib

  class TagadaGraph < SealedClass
    sub_classes "Specification", "Differential", "TruncatedDifferential"
  end

  class SpecificationGraph < TagadaGraph
    sealed!("Specification")
    attribute :graph, Specification::SpecificationGraph
  end

  class DifferentialGraph < TagadaGraph
    sealed!("Differential")
    attribute :graph, Differential::DifferentialGraph
  end

  class TruncatedDifferentialGraph < TagadaGraph
    sealed!("TruncatedDifferential")
    attribute :graph, TruncatedDifferential::TruncatedDifferentialGraph
  end
end
