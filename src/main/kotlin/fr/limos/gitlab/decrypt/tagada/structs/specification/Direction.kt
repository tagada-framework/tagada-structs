package fr.limos.gitlab.decrypt.tagada.structs.specification

/**
 * Shift directions used in the LFSR data structure
 */
enum class Direction { ShiftLeft, ShiftRight }