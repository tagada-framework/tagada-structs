package fr.limos.gitlab.decrypt.tagada.structs.differential

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer

@JsonSerialize(using = Hundredth.Serializer::class)
@JsonDeserialize(using = Hundredth.Deserializer::class)
data class Hundredth(val value: Int) : Comparable<Hundredth> {
    override fun toString() = "%.2f".format(value / 100.0)

    override fun compareTo(other: Hundredth) = value.compareTo(other.value)

    class Serializer(t: Class<Hundredth>? = null): StdSerializer<Hundredth>(t) {
        override fun serialize(value: Hundredth, gen: JsonGenerator, provider: SerializerProvider) =
            gen.writeNumber(value.value)
    }

    class Deserializer(c: Class<*>? = null): StdDeserializer<Hundredth>(c) {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext) =
            Hundredth(p.intValue)
    }
}