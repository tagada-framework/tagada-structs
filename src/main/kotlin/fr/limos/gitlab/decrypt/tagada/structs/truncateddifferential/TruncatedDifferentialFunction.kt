package fr.limos.gitlab.decrypt.tagada.structs.truncateddifferential

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonTypeInfo
import fr.limos.gitlab.decrypt.tagada.structs.differential.MinusLog2Probability


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
sealed class TruncatedDifferentialFunction

data class NonLinearTransition(
    @JsonProperty("maximum_activation_probability")
    val maximumActivationProbability: MinusLog2Probability
) : TruncatedDifferentialFunction()
data class Linear(
    @JsonProperty("truth_table")
    val truthTable: List<List<Boolean>>
) : TruncatedDifferentialFunction()

