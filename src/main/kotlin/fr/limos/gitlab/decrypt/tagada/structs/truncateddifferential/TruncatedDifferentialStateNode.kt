package fr.limos.gitlab.decrypt.tagada.structs.truncateddifferential

import com.fasterxml.jackson.annotation.JsonTypeInfo
import fr.limos.gitlab.decrypt.tagada.structs.common.Domain
import fr.limos.gitlab.decrypt.tagada.structs.common.Value

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
sealed class TruncatedDifferentialStateNode

data class Variable(val domain: Domain): TruncatedDifferentialStateNode()
data class Constant(val value: Value): TruncatedDifferentialStateNode()