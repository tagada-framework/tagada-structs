package fr.limos.gitlab.decrypt.tagada.structs.specification

/**
 * Represents a function call in the cipher in the form of function(inputs) = outputs
 */
data class Transition(
    val inputs: List<StateNodeUID>,
    val function: FunctionNodeUID,
    val outputs: List<StateNodeUID>
)