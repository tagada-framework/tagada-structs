package fr.limos.gitlab.decrypt.tagada.structs.differential

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import fr.limos.gitlab.decrypt.tagada.structs.common.Value
import com.fasterxml.jackson.databind.KeyDeserializer as JsonDeserializer

data class DDTArgs(val deltaIn: Value, val deltaOut: Value) {
    class KeySerializer: JsonSerializer<DDTArgs>() {
        override fun serialize(
            value: DDTArgs,
            gen: JsonGenerator,
            serializers: SerializerProvider
        ) {
            gen.writeFieldName("(${value.deltaIn},${value.deltaOut})")
        }
    }
    class KeyDeserializer: JsonDeserializer() {
        override fun deserializeKey(key: String, ctxt: DeserializationContext): DDTArgs {
            val (deltaIn, deltaOut) = key.subSequence(1, key.length - 1).split(',').map(String::toInt)
            return DDTArgs(deltaIn, deltaOut)
        }
    }
}