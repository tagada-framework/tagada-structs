package fr.limos.gitlab.decrypt.tagada.structs.differential

import fr.limos.gitlab.decrypt.tagada.structs.common.Version

/**
 * Represents a cipher Dag (directed acyclic graph) in Tagada.
 * @param version the library version that generates the JSON
 * @param states the states of the cipher. The states include the plaintext, the key, the ciphertext, the internal
 * states and the constants of the cipher
 * @param functions the functions used in the cipher
 * @param transitions the links between the states using one of the cipher function
 */
data class DifferentialGraph(
    val version: Version,
    val states: List<DifferentialStateNode>,
    val functions: List<DifferentialFunctionNode>,
    val transitions: List<DifferentialTransition>,
    val plaintext: List<DifferentialStateNodeUID>,
    val key: List<DifferentialStateNodeUID>,
    val ciphertext: List<DifferentialStateNodeUID>,
    val names: Map<String, DifferentialStateNodeUID>,
    val blocks: Map<String, List<DifferentialStateNodeUID>>,
)