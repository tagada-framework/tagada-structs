package fr.limos.gitlab.decrypt.tagada.structs.differential

import com.fasterxml.jackson.annotation.JsonTypeInfo
import fr.limos.gitlab.decrypt.tagada.structs.common.Domain
import fr.limos.gitlab.decrypt.tagada.structs.common.Value

/**
 * Represents a state of the cipher can be either a variable or a constant.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
sealed class DifferentialStateNode

data class Probability(val domain: Set<MinusLog2Probability>) : DifferentialStateNode()
data class Variable(val domain: Domain) : DifferentialStateNode()
data class Constant(val value: Value) : DifferentialStateNode()