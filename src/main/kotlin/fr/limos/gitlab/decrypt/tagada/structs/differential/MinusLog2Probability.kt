package fr.limos.gitlab.decrypt.tagada.structs.differential

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer

@JsonSerialize(using = MinusLog2Probability.Serializer::class)
@JsonDeserialize(using = MinusLog2Probability.Deserializer::class)
data class MinusLog2Probability(val hundredth: Hundredth) : Comparable<MinusLog2Probability> {
    override fun toString() = "2^{-$hundredth}"

    override fun compareTo(other: MinusLog2Probability) = -hundredth.compareTo(other.hundredth)


    class Serializer(t: Class<MinusLog2Probability>? = null) : StdSerializer<MinusLog2Probability>(t) {
        override fun serialize(value: MinusLog2Probability, gen: JsonGenerator, provider: SerializerProvider) =
            gen.writeObject(value.hundredth)
    }

    class Deserializer(c: Class<*>? = null) : StdDeserializer<MinusLog2Probability>(c) {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext) =
            MinusLog2Probability(p.readValueAs(Hundredth::class.java))
    }
}