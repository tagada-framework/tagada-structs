package fr.limos.gitlab.decrypt.tagada.structs.truncateddifferential

import fr.limos.gitlab.decrypt.tagada.structs.common.Version


data class TruncatedDifferentialGraph(
    val version: Version,
    val states: List<TruncatedDifferentialStateNode>,
    val functions: List<TruncatedDifferentialFunctionNode>,
    val transitions: List<TruncatedDifferentialTransition>,
    val plaintext: List<TruncatedDifferentialStateNodeUID>,
    val key: List<TruncatedDifferentialStateNodeUID>,
    val ciphertext: List<TruncatedDifferentialFunctionNodeUID>,
    val names: Map<String, TruncatedDifferentialFunctionNodeUID>,
    val blocks: Map<String, List<TruncatedDifferentialFunctionNodeUID>>
)



