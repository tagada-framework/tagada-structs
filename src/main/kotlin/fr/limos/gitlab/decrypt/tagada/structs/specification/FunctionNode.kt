package fr.limos.gitlab.decrypt.tagada.structs.specification

import com.fasterxml.jackson.annotation.JsonProperty
import fr.limos.gitlab.decrypt.tagada.structs.common.Domain

/**
 * Defines a function node. A function is defined by a domain, a co-domain and a mapping.
 * @param domain the domain of the function is represented by a cartesian product of the domain of the input parameters
 * @param coDomain the domain of the function is represented by a cartesian product of the domain of the output parameters
 * @param function the mapping function
 */
data class FunctionNode(
    val domain: List<Domain>,
    @JsonProperty("co_domain")
    val coDomain: List<Domain>,
    val function: Function
)