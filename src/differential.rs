use std::cmp::Ordering;
use std::collections::{BTreeMap, BTreeSet, HashMap};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use crate::common::{Domain, Value, Version};
use crate::specification::LinearFunction;

/// Represents a cipher Dag (directed acyclic graph) in Tagada.
/// - `version`: the library version that generates the JSON
/// - `states`: the states of the cipher. The states include the plaintext, the key, the ciphertext, the internal
/// - `states`: and the constants of the cipher
/// - `functions`: the functions used in the cipher
/// - `transitions`: the links between the states using one of the cipher function
#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub struct DifferentialGraph {
    pub version: Version,
    pub states: Vec<DifferentialStateNode>,
    pub functions: Vec<DifferentialFunctionNode>,
    pub transitions: Vec<DifferentialTransition>,
    pub plaintext: Vec<DifferentialStateNodeUID>,
    pub key: Vec<DifferentialStateNodeUID>,
    pub ciphertext: Vec<DifferentialStateNodeUID>,
    pub names: HashMap<String, DifferentialStateNodeUID>,
    pub blocks: HashMap<String, Vec<DifferentialStateNodeUID>>,
}

/// Represents a differential state of the cipher
/// - `domain`: the accepted domain of the differential state
/// - `value`: the value of the state. When the differential state is a constant, the value must be set to a value that belongs to
/// the domain. If the state is a constant, the value must be set to None.
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
#[serde(tag = "type")]
pub enum DifferentialStateNode {
    Variable { domain: Domain },
    Probability { domain: BTreeSet<MinusLog2Probability> },
    Constant { value: Value },
}

/// Defines a function node. A function is defined by a domain, a co-domain and a mapping.
/// - `domain`: the domain of the function is represented by a cartesian product of the domain of the input parameters
/// - `co_domain`: the domain of the function is represented by a cartesian product of the domain of the output parameters
/// - `function`: the mapping function
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
pub struct DifferentialFunctionNode {
    pub domain: Vec<Domain>,
    pub co_domain: Vec<Domain>,
    pub function: DifferentialFunction,
}

/// Represents the managed functions.
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
#[serde(tag = "type")]
pub enum DifferentialFunction {
    DDT {
        #[serde(with = "ddt_map")]
        probabilities: BTreeMap<(Value, Value), MinusLog2Probability>
    },
    Linear { linear: LinearFunction },
}

/// Represents a function call in the cipher in the form of function(inputs) = outputs
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
pub struct DifferentialTransition {
    pub inputs: Vec<DifferentialStateNodeUID>,
    pub function: DifferentialFunctionNodeUID,
    pub outputs: Vec<DifferentialStateNodeUID>,
}

/// A function node UID
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub struct DifferentialFunctionNodeUID {
    pub uid: usize,
}

impl Serialize for DifferentialFunctionNodeUID {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        self.uid.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for DifferentialFunctionNodeUID {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        Deserialize::deserialize(deserializer)
            .map(|uid: usize| DifferentialFunctionNodeUID { uid })
    }
}

/// A function node UID
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub struct DifferentialStateNodeUID {
    pub uid: usize,
}

impl Serialize for DifferentialStateNodeUID {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        self.uid.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for DifferentialStateNodeUID {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        Deserialize::deserialize(deserializer)
            .map(|uid: usize| DifferentialStateNodeUID { uid })
    }
}

#[derive(Serialize, Deserialize, Eq, PartialEq, Hash, Clone, Debug)]
pub struct DifferentialComputation(pub Vec<(Vec<Value>, MinusLog2Probability)>);

///
/// Represents a probability with its -log2 value.
///
/// **Examples:**
///
/// - $1 = 2^{-0}$ is represented by `Some(MinusLog2Probability(Hundredth(0)))`
/// - $2^{-8}$ is represented by `Some(800)`
/// - $2^{-3.4}$ is represented by `Some(340)`
/// - $0$ is represented by `None`
///
#[derive(Eq, PartialEq, Hash, Copy, Clone, Debug)]
pub struct MinusLog2Probability(pub Hundredth);

impl Ord for MinusLog2Probability {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0).reverse()
    }
}

impl PartialOrd for MinusLog2Probability {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Serialize for MinusLog2Probability {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        self.0.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for MinusLog2Probability {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        Deserialize::deserialize(deserializer)
            .map(|hundredth: Hundredth| MinusLog2Probability(hundredth))
    }
}

#[derive(Eq, PartialEq, Hash, Copy, Clone, Debug, Ord, PartialOrd)]
pub struct Hundredth(pub usize);

impl Serialize for Hundredth {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        self.0.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for Hundredth {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        Deserialize::deserialize(deserializer)
            .map(|value: usize| Hundredth(value))
    }
}

mod ddt_map {
    use std::collections::BTreeMap;
    use std::fmt::{Formatter};
    use serde::{Deserializer, Serializer};
    use serde::de::{MapAccess, Visitor};
    use serde::ser::SerializeMap;
    use crate::common::Value;
    use crate::differential::MinusLog2Probability;

    type DDTMap = BTreeMap<(Value, Value), MinusLog2Probability>;

    fn to_string(key: &(Value, Value)) -> String {
        let mut s = String::new();
        s.push('(');
        s.push_str(&key.0.to_string());
        s.push(',');
        s.push_str(&key.1.to_string());
        s.push(')');
        s
    }

    fn from_str(key: &String) -> (Value, Value) {
        let fields = key[1..key.len() - 1].split(',').map(|it| it.parse().unwrap()).collect::<Vec<_>>();
        (fields[0], fields[1])
    }

    struct DDTMapVisitor;

    impl<'de> Visitor<'de> for DDTMapVisitor {
        type Value = DDTMap;

        fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
            write!(formatter, "invalid DDT map format")
        }

        fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error> where A: MapAccess<'de> {
            let mut result = BTreeMap::new();
            while let Some((key_str, value)) = map.next_entry()? {
                let key = from_str(&key_str);
                result.insert(key, value);
            }
            Ok(result)
        }
    }

    pub(super) fn serialize<S: Serializer>(attr: &DDTMap, ser: S) -> Result<S::Ok, S::Error> {
        let mut map_ser = ser.serialize_map(Some(attr.len()))?;
        for (key, value) in attr.iter() {
            map_ser.serialize_entry(&to_string(key), value)?;
        }
        map_ser.end()
    }

    pub(super) fn deserialize<'de, D: Deserializer<'de>>(des: D) -> Result<DDTMap, D::Error> {
        des.deserialize_map(DDTMapVisitor {})
    }
}

#[cfg(test)]
mod tests {
    use std::collections::{BTreeMap, BTreeSet};
    use std::io;
    use serde_json::to_string;
    use crate::common::Domain::Range;
    use crate::common::tests::{read_json_from, read_text_from};
    use crate::differential::DifferentialStateNode::Probability;
    use crate::differential::{DifferentialFunction, DifferentialFunctionNode, DifferentialFunctionNodeUID, DifferentialStateNode, Hundredth, MinusLog2Probability};
    use crate::differential::DifferentialFunction::Linear;
    use crate::specification::LinearFunction;

    #[test]
    fn serialize_ddt() -> io::Result<()> {
        let mut probabilities = BTreeMap::new();
        probabilities.insert((0, 0), MinusLog2Probability(Hundredth(0)));
        probabilities.insert((1, 8), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((1, 9), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((1, 10), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((1, 11), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((2, 1), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((2, 3), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((2, 5), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((2, 6), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((3, 8), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 9), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 10), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 11), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((4, 2), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((4, 6), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((4, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((4, 11), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((4, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((4, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((5, 2), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((5, 6), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((5, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((5, 10), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((5, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((5, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 1), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 3), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 4), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 8), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 10), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 1), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 3), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 4), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 9), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 11), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((8, 4), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((8, 5), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((8, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((8, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((8, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((8, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((9, 4), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((9, 5), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((9, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((9, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((9, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((9, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((10, 5), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((10, 6), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((10, 8), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((10, 9), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((10, 10), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((10, 11), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((11, 1), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((11, 3), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((11, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((11, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((11, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((11, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((12, 2), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((12, 6), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((12, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((12, 8), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((12, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((12, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((13, 2), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((13, 6), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((13, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((13, 9), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((13, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((13, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 1), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 3), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 4), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 9), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 11), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 1), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 3), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 4), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 8), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 10), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 15), MinusLog2Probability(Hundredth(300)));
        let skinny_ddt = DifferentialFunction::DDT {
            probabilities
        };
        assert_eq!(read_text_from("differential/ddt")?, to_string(&skinny_ddt)?);
        Ok(())
    }

    #[test]
    fn deserialize_ddt() -> io::Result<()> {
        let mut probabilities = BTreeMap::new();
        probabilities.insert((0, 0), MinusLog2Probability(Hundredth(0)));
        probabilities.insert((1, 8), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((1, 9), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((1, 10), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((1, 11), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((2, 1), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((2, 3), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((2, 5), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((2, 6), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((3, 8), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 9), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 10), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 11), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((3, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((4, 2), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((4, 6), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((4, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((4, 11), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((4, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((4, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((5, 2), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((5, 6), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((5, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((5, 10), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((5, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((5, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 1), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 3), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 4), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 8), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 10), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((6, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 1), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 3), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 4), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 9), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 11), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((7, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((8, 4), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((8, 5), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((8, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((8, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((8, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((8, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((9, 4), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((9, 5), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((9, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((9, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((9, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((9, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((10, 5), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((10, 6), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((10, 8), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((10, 9), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((10, 10), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((10, 11), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((11, 1), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((11, 3), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((11, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((11, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((11, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((11, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((12, 2), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((12, 6), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((12, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((12, 8), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((12, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((12, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((13, 2), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((13, 6), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((13, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((13, 9), MinusLog2Probability(Hundredth(200)));
        probabilities.insert((13, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((13, 15), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 1), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 3), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 4), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 9), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 11), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 13), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((14, 14), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 1), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 3), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 4), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 7), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 8), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 10), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 12), MinusLog2Probability(Hundredth(300)));
        probabilities.insert((15, 15), MinusLog2Probability(Hundredth(300)));
        let skinny_ddt = DifferentialFunction::DDT {
            probabilities
        };
        assert_eq!(skinny_ddt, read_json_from("differential/ddt").unwrap());
        Ok(())
    }

    #[test]
    fn serialize_linear() -> io::Result<()> {
        let equal = Linear { linear: LinearFunction::Equal };
        assert_eq!(read_text_from("differential/linear")?, to_string(&equal)?);
        Ok(())
    }

    #[test]
    fn deserialize_linear() -> io::Result<()> {
        let equal = Linear { linear: LinearFunction::Equal };
        assert_eq!(equal, read_json_from("differential/linear")?);
        Ok(())
    }

    #[test]
    fn serialize_differential_function_node() -> io::Result<()> {
        let byte = Range { min: 0, max: 256 };
        let function_node = DifferentialFunctionNode {
            domain: vec![byte.clone()],
            co_domain: vec![byte.clone()],
            function: Linear { linear: LinearFunction::Equal },
        };
        assert_eq!(read_text_from("differential/function_node")?, to_string(&function_node)?);
        Ok(())
    }


    #[test]
    fn deserialize_differential_function_node() -> io::Result<()> {
        let byte = Range { min: 0, max: 256 };
        let function_node = DifferentialFunctionNode {
            domain: vec![byte.clone()],
            co_domain: vec![byte.clone()],
            function: Linear { linear: LinearFunction::Equal },
        };
        assert_eq!(function_node, read_json_from("differential/function_node")?);
        Ok(())
    }

    #[test]
    fn serialize_differential_function_node_uid() -> io::Result<()> {
        let f_uid = DifferentialFunctionNodeUID { uid: 10 };
        assert_eq!(read_text_from("function_node_uid")?, to_string(&f_uid)?);
        Ok(())
    }

    #[test]
    fn deserialize_differential_function_node_uid() -> io::Result<()> {
        let f_uid = DifferentialFunctionNodeUID { uid: 10 };
        assert_eq!(f_uid, read_json_from("function_node_uid")?);
        Ok(())
    }

    #[test]
    fn serialize_differential_variable() -> io::Result<()> {
        let variable = DifferentialStateNode::Variable {
            domain: Range { min: 0, max: 256 }
        };
        assert_eq!(read_text_from("differential/variable")?, to_string(&variable)?);
        Ok(())
    }

    #[test]
    fn deserialize_differential_variable() -> io::Result<()> {
        let variable = DifferentialStateNode::Variable {
            domain: Range { min: 0, max: 256 }
        };
        assert_eq!(variable, read_json_from("differential/variable")?);
        Ok(())
    }

    #[test]
    fn serialize_differential_probability() -> io::Result<()> {
        let mut domain = BTreeSet::new();
        domain.insert(MinusLog2Probability(Hundredth(0)));
        domain.insert(MinusLog2Probability(Hundredth(600)));
        domain.insert(MinusLog2Probability(Hundredth(700)));
        let probability = Probability { domain };
        assert_eq!(read_text_from("differential/probability")?, to_string(&probability)?);
        Ok(())
    }

    #[test]
    fn deserialize_differential_probability() -> io::Result<()> {
        let mut domain = BTreeSet::new();
        domain.insert(MinusLog2Probability(Hundredth(0)));
        domain.insert(MinusLog2Probability(Hundredth(600)));
        domain.insert(MinusLog2Probability(Hundredth(700)));
        let probability = Probability { domain };
        assert_eq!(probability, read_json_from("differential/probability")?);
        Ok(())
    }

    #[test]
    fn serialize_differential_constant() -> io::Result<()> {
        let constant = DifferentialStateNode::Constant { value: 13 };
        assert_eq!(read_text_from("differential/constant")?, to_string(&constant)?);
        Ok(())
    }

    #[test]
    fn deserialize_differential_constant() -> io::Result<()> {
        let constant = DifferentialStateNode::Constant { value: 13 };
        assert_eq!(constant, read_json_from("differential/constant")?);
        Ok(())
    }
}
