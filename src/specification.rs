use std::collections::HashMap;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use crate::common::{Domain, Value, Version};
use crate::computation::{and_const, bitwise_xor, circular_shift, concat, equal, gf_mat_to_vec_mul, gf_mul, lfsr, or_const, sbox, shift, split, table};
use crate::computation::{and_const_into, bitwise_xor_into, circular_shift_into, concat_into, equal_into, gf_mat_to_vec_mul_into, gf_mul_into, lfsr_into, or_const_into, sbox_into, shift_into, split_into, table_into};

/// Represents a cipher Dag (directed acyclic graph) in Tagada.
/// - `version`: the library version that generates the JSON
/// - `states`: the states of the cipher. The states include the plaintext, the key, the ciphertext, the internal
/// - `states`: and the constants of the cipher
/// - `functions`: the functions used in the cipher
/// - `transitions`: the links between the states using one of the cipher function
#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub struct SpecificationGraph {
    pub version: Version,
    pub states: Vec<StateNode>,
    pub functions: Vec<FunctionNode>,
    pub transitions: Vec<Transition>,
    pub plaintext: Vec<StateNodeUID>,
    pub key: Vec<StateNodeUID>,
    pub ciphertext: Vec<StateNodeUID>,
    pub names: HashMap<String, StateNodeUID>,
    pub blocks: HashMap<String, Vec<StateNodeUID>>,
}

/// Represents a state of the cipher
/// - `domain`: the accepted domain of the state
/// - `value`: the value of the state. When the state is a constant, the value must be set to a value that belongs to
/// the domain. If the state is a constant, the value must be set to None.
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
#[serde(tag = "type")]
pub enum StateNode {
    Variable { domain: Domain },
    Constant { value: Value },
}

impl StateNode {
    pub fn is_constant(&self) -> bool {
        match self {
            StateNode::Constant { .. } => true,
            _ => false
        }
    }

    pub fn is_variable(&self) -> bool {
        match self {
            StateNode::Variable { .. } => true,
            _ => false
        }
    }
}

/// Defines a function node. A function is defined by a domain, a co-domain and a mapping.
/// - `domain`: the domain of the function is represented by a cartesian product of the domain of the input parameters
/// - `co_domain`: the domain of the function is represented by a cartesian product of the domain of the output parameters
/// - `function`: the mapping function
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
pub struct FunctionNode {
    pub domain: Vec<Domain>,
    pub co_domain: Vec<Domain>,
    pub function: Function,
}

pub trait Computable {
    fn call(&self, args: &Vec<Value>) -> Vec<Value>;
    fn call_into(&self, args: &Vec<Value>, output: &mut Vec<Value>);
}

impl Computable for Function {
    fn call(&self, args: &Vec<Value>) -> Vec<Value> {
        match self {
            Function::SBox { lookup_table } => sbox(lookup_table, args[0]),
            Function::Linear { linear: inner } => inner.call(args)
        }
    }

    fn call_into(&self, args: &Vec<Value>, output: &mut Vec<Value>) {
        match self {
            Function::SBox { lookup_table } => sbox_into(lookup_table, args[0], output),
            Function::Linear { linear: inner } => inner.call_into(args, output)
        }
    }
}

impl Computable for LinearFunction {
    fn call(&self, args: &Vec<Value>) -> Vec<Value> {
        match self {
            LinearFunction::Equal => equal(args[0]),
            LinearFunction::BitwiseXOR => bitwise_xor(args),
            LinearFunction::Table { tuples } => table(tuples, args),
            LinearFunction::Command { executable: _ } => panic!("Not implemented."),
            LinearFunction::GFMul { poly, constant } => gf_mul(*poly, args[0], *constant),
            LinearFunction::GFMatToVecMul { poly, m } => gf_mat_to_vec_mul(*poly, m, args),
            LinearFunction::LFSR { nb_bits, poly, direction } => lfsr(*nb_bits, *poly, direction, args[0]),
            LinearFunction::Split { to_words } => split(to_words, args[0]),
            LinearFunction::Concat { to_word } => concat(to_word, args),
            LinearFunction::Shift { nb_bits, bit_count, direction } => shift(*nb_bits, *bit_count, direction, args[0]),
            LinearFunction::CircularShift { nb_bits, bit_count, direction } => circular_shift(*nb_bits, *bit_count, direction, args[0]),
            LinearFunction::AndConst { constant } => and_const(*constant, args[0]),
            LinearFunction::OrConst { constant } => or_const(*constant, args[0]),
        }
    }

    fn call_into(&self, args: &Vec<Value>, output: &mut Vec<Value>) {
        match self {
            LinearFunction::Equal => equal_into(args[0], output),
            LinearFunction::BitwiseXOR => bitwise_xor_into(args, output),
            LinearFunction::Table { tuples } => table_into(tuples, args, output),
            LinearFunction::Command { executable: _ } => panic!("Not implemented."),
            LinearFunction::GFMul { poly, constant } => gf_mul_into(*poly, args[0], *constant, output),
            LinearFunction::GFMatToVecMul { poly, m } => gf_mat_to_vec_mul_into(*poly, m, args, output),
            LinearFunction::LFSR { nb_bits, poly, direction } => lfsr_into(*nb_bits, *poly, direction, args[0], output),
            LinearFunction::Split { to_words } => split_into(to_words, args[0], output),
            LinearFunction::Concat { to_word } => concat_into(to_word, args, output),
            LinearFunction::Shift { nb_bits, bit_count, direction } => shift_into(*nb_bits, *bit_count, direction, args[0], output),
            LinearFunction::CircularShift { nb_bits, bit_count, direction } => circular_shift_into(*nb_bits, *bit_count, direction, args[0], output),
            LinearFunction::AndConst { constant } => and_const_into(*constant, args[0], output),
            LinearFunction::OrConst { constant } => or_const_into(*constant, args[0], output),
        }
    }
}

/// Represents the managed functions.
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
#[serde(tag = "type")]
pub enum Function {
    /// Represents a **NON-LINEAR** unary function using a lookup table
    /// - `lookup_table`: the lookup table used by the operator
    /// #### Examples
    /// ```rust
    /// # use tagada_structs::specification::Computable;
    /// # use tagada_structs::specification::Function::SBox;
    /// let lookup_table = vec![ 1, 3, 2, 0 ];
    /// let sbox = SBox { lookup_table  };
    /// assert_eq!(sbox.call(&vec![0]), vec![1] );
    /// assert_eq!(sbox.call(&vec![1]), vec![3] );
    /// assert_eq!(sbox.call(&vec![2]), vec![2] );
    /// assert_eq!(sbox.call(&vec![3]), vec![0] );
    /// ```
    SBox { lookup_table: Vec<Value> },
    Linear { linear: LinearFunction },
}

/// Represents the managed functions.
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
#[serde(tag = "type")]
pub enum LinearFunction {
    /// Represents the equal ( \(=\) ) operator
    /// #### Examples
    /// ```rust
    /// # use tagada_structs::specification::Computable;
    /// # use tagada_structs::specification::LinearFunction::Equal;
    /// assert_eq!(Equal.call(&vec![10]), vec![10]);
    /// ```
    Equal,
    /// Represents the bitwise XOR ( \( \oplus\) ) operator
    /// #### Examples
    /// ```rust
    /// # use tagada_structs::specification::Computable;
    /// # use tagada_structs::specification::LinearFunction::BitwiseXOR;
    /// assert_eq!(BitwiseXOR.call(&vec![ 0x53, 0xCA ]), vec![ 0x99 ]);
    /// ```
    BitwiseXOR,
    /// Represents a function in extension. The inputs must represent a primary key in the tuples, i.e.
    /// it must have exactly **one** tuple that contains the input sequence.
    /// - `tuples`: the list of valid tuples.
    /// ##### Examples
    ///
    /// - Valid usage:
    /// ```rust
    /// # use tagada_structs::specification::Computable;
    /// # use tagada_structs::specification::LinearFunction::Table;
    /// let tuples = vec![
    ///     vec![0, 0, 0],
    ///     vec![0, 1, 1],
    ///     vec![1, 0, 1],
    ///     vec![1, 1, 0],
    /// ];
    /// let table = Table { tuples };
    /// assert_eq!(table.call(&vec![ 0, 0 ]), vec![0]);
    /// assert_eq!(table.call(&vec![ 0, 1 ]), vec![1]);
    /// assert_eq!(table.call(&vec![ 1, 0 ]), vec![1]);
    /// assert_eq!(table.call(&vec![ 1, 1 ]), vec![0]);
    /// ```
    /// - Invalid usage
    /// ```rust
    /// # use std::panic::catch_unwind;
    /// # use tagada_structs::specification::Computable;
    /// # use tagada_structs::specification::LinearFunction::Table;
    /// let tuples = vec![
    ///     vec![0, 0, 0],
    ///     vec![0, 1, 1],
    ///     vec![1, 0, 1],
    ///     vec![1, 1, 0],
    /// ];
    /// let table = Table { tuples };
    /// assert!(catch_unwind(|| table.call(&vec![ 0 ])).is_err());
    /// // [0] is not valid since two entries start with [ 0, ... ]
    /// assert!(catch_unwind(|| table.call(&vec![ 3 ])).is_err());
    /// // [3] is not valid since no entry start with [ 3, ... ]
    /// ```
    Table { tuples: Vec<Vec<Value>> },
    /// Represents a black box operator as an executable path. The executable must accept the function argument as inline
    /// arguments, e.g. `times 2 3` and must return the values separated by a single space, e.g. `4 5 6`.
    Command { executable: String },
    /// Represents a Galois Field multiplication using the given irreducible polynomial
    /// - `poly`: the polynomial used to perform the multiplication
    /// ##### Examples
    /// ```rust
    /// # use tagada_structs::specification::Computable;
    /// # use tagada_structs::specification::LinearFunction::GFMul;
    /// assert_eq!(GFMul { poly: 0x11B, constant: 0x53 }.call(&vec![ 0xCA ]), vec![ 0x01 ]);
    /// ```
    GFMul { poly: usize, constant: Value },
    /// Represents a Galois Field matrix to column vector multiplication using the given irreducible polynomial
    /// - `poly`: the polynomial used to perform the underlying galois field multiplication
    /// - `m`: the multiplication matrix
    GFMatToVecMul { poly: usize, m: Vec<Vec<Value>> },
    /// Represents a LFSR function (https://en.wikipedia.org/wiki/Linear-feedback_shift_register)
    /// The function output is equal to a cyclic shift (either Left or Right) of its internal register.
    /// For a left (resp. right) shift, the rightmost (resp. leftmost) bit is overridden by a XOR between the internal
    /// register and a polynomial.
    /// - `nb_bits`: The number of bits of the LFSR
    /// - `poly`: The polynomial used in the LFSR
    /// - `direction`: The shift direction
    /// ##### Examples
    /// ```rust
    /// # use tagada_structs::specification::Computable;
    /// # use tagada_structs::specification::Direction::ShiftRight;
    /// # use tagada_structs::specification::LinearFunction::LFSR;
    /// # let x0 = 0b0001;
    /// # let x1 = 0b0010;
    /// # let x2 = 0b0100;
    /// # let x3 = 0b1000;
    /// let lfsr = LFSR { nb_bits: 4, poly: x3 ^ x2 ^ x0, direction: ShiftRight };
    /// assert_eq!(lfsr.call(&vec![ 0b0110 ]), vec![ 0b1011 ]);
    /// assert_eq!(lfsr.call(&vec![ 0b1011 ]), vec![ 0b0101 ]);
    /// assert_eq!(lfsr.call(&vec![ 0b0101 ]), vec![ 0b0010 ]);
    /// assert_eq!(lfsr.call(&vec![ 0b0010 ]), vec![ 0b0001 ]);
    /// assert_eq!(lfsr.call(&vec![ 0b0001 ]), vec![ 0b1000 ]);
    /// assert_eq!(lfsr.call(&vec![ 0b1000 ]), vec![ 0b1100 ]);
    /// assert_eq!(lfsr.call(&vec![ 0b1100 ]), vec![ 0b0110 ]);
    /// ```
    ///
    LFSR { nb_bits: usize, poly: usize, direction: Direction },
    /// Represents a split from one entry to several outputs.
    /// - `to_words`: The words mapping. Indicates for each bit in which output word it must belong.
    /// ##### Examples
    /// ```rust
    /// # use tagada_structs::specification::Computable;
    /// # use tagada_structs::specification::LinearFunction::Split;
    /// assert_eq!(
    ///     Split { to_words: vec![vec![7,6,5,4], vec![3,2,1,0]] }.call(&vec![ 0b0110_1001 ]),
    ///     vec![0b0110, 0b1001]
    /// );
    /// assert_eq!(
    ///     Split { to_words: vec![vec![3,2,1,0], vec![7,6,5,4]] }.call(&vec![ 0b0110_1001 ]),
    ///     vec![0b1001, 0b0110]
    /// );
    /// assert_eq!(
    ///     Split { to_words: vec![vec![7,6], vec![1,0], vec![3,2], vec![5,4]] }.call(&vec![ 0b0110_1001 ]),
    ///     vec![0b01, 0b01, 0b10, 0b10]
    /// );
    /// ```
    Split { to_words: Vec<Vec<usize>> },
    /// Represents a split from one entry to several outputs.
    /// - `to_word`: The word mapping. Indicates for each bit its position in the inputs words.
    /// ##### Examples
    /// ```rust
    /// # use tagada_structs::specification::{BitAndWord, Computable};
    /// # use tagada_structs::specification::LinearFunction::Concat;
    /// assert_eq!(
    ///     Concat { to_word: vec![
    ///         BitAndWord { bit_pos: 3, word_pos: 0 },
    ///         BitAndWord { bit_pos: 2, word_pos: 0 },
    ///         BitAndWord { bit_pos: 1, word_pos: 0 },
    ///         BitAndWord { bit_pos: 0, word_pos: 0 },
    ///         BitAndWord { bit_pos: 3, word_pos: 1 },
    ///         BitAndWord { bit_pos: 2, word_pos: 1 },
    ///         BitAndWord { bit_pos: 1, word_pos: 1 },
    ///         BitAndWord { bit_pos: 0, word_pos: 1 },
    ///     ] }.call(&vec![0b0110, 0b1001]),
    ///     vec![ 0b0110_1001 ]
    /// );
    /// assert_eq!(
    ///     Concat { to_word: vec![
    ///         BitAndWord { bit_pos: 3, word_pos: 0 },
    ///         BitAndWord { bit_pos: 2, word_pos: 1 },
    ///         BitAndWord { bit_pos: 1, word_pos: 1 },
    ///         BitAndWord { bit_pos: 0, word_pos: 0 },
    ///     ] }.call(&vec![0b0110, 0b1001]),
    ///     vec![ 0b0000 ]
    /// );
    /// ```
    Concat { to_word: Vec<BitAndWord> },
    /// Represents a register shift. Bits beyond the register are lost. Moved bits are replaced by zero.
    /// - `nb_bits`: The size of the register.
    /// - `bit_count`: The number of bits to shift.
    /// - `direction`: The shift direction (left or right).
    /// ##### Examples
    /// ```rust
    /// # use tagada_structs::specification::{BitAndWord, Computable, Direction};
    /// # use tagada_structs::specification::LinearFunction::Shift;
    /// assert_eq!(
    ///     Shift {
    ///         nb_bits: 5,
    ///         bit_count: 2,
    ///         direction: Direction::ShiftLeft
    ///     }.call(&vec![ 0b10110 ]),
    ///     vec![ 0b11000 ]
    /// );
    /// assert_eq!(
    ///     Shift {
    ///         nb_bits: 5,
    ///         bit_count: 2,
    ///         direction: Direction::ShiftRight
    ///     }.call(&vec![ 0b10110 ]),
    ///     vec![ 0b00101 ]
    /// );
    /// ```
    Shift { nb_bits: usize, bit_count: usize, direction: Direction },
    /// Represents a circular register shift. Bits beyond the register reintroduced into the register.
    /// - `nb_bits`: The size of the register.
    /// - `bit_count`: The number of bits to shift.
    /// - `direction`: The shift direction (left or right).
    /// ##### Examples
    /// ```rust
    /// # use tagada_structs::specification::{BitAndWord, Computable, Direction};
    /// # use tagada_structs::specification::LinearFunction::CircularShift;
    /// assert_eq!(
    ///     CircularShift {
    ///         nb_bits: 5,
    ///         bit_count: 3,
    ///         direction: Direction::ShiftLeft
    ///     }.call(&vec![ 0b10110 ]),
    ///     vec![ 0b10101 ]
    /// );
    /// assert_eq!(
    ///     CircularShift {
    ///         nb_bits: 5,
    ///         bit_count: 3,
    ///         direction: Direction::ShiftRight
    ///     }.call(&vec![ 0b10101 ]),
    ///     vec![ 0b10110 ]
    /// );
    /// ```
    CircularShift { nb_bits: usize, bit_count: usize, direction: Direction },
    /// Represents a Bitwise And with a constant
    /// - `nb_bits`: A constant value.
    /// ##### Examples
    /// ```rust
    /// # use tagada_structs::specification::{BitAndWord, Computable, Direction};
    /// # use tagada_structs::specification::LinearFunction::AndConst;
    /// assert_eq!(
    ///     AndConst {
    ///         constant: 0b111_000,
    ///     }.call(&vec![ 0b101_010 ]),
    ///     vec![ 0b101_000 ]
    /// );
    AndConst { constant: Value },
    /// Represents a Bitwise Or with a constant
    /// - `nb_bits`: A constant value.
    /// ##### Examples
    /// ```rust
    /// # use tagada_structs::specification::{BitAndWord, Computable, Direction};
    /// # use tagada_structs::specification::LinearFunction::OrConst;
    /// assert_eq!(
    ///     OrConst {
    ///         constant: 0b111_000,
    ///     }.call(&vec![ 0b101_010 ]),
    ///     vec![ 0b111_010 ]
    /// );
    OrConst { constant: Value },
}

#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub struct BitAndWord {
    pub bit_pos: usize,
    pub word_pos: usize,
}

impl Serialize for BitAndWord {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        (self.bit_pos, self.word_pos).serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for BitAndWord {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        Deserialize::deserialize(deserializer)
            .map(|(bit_pos, word_pos)| BitAndWord { bit_pos, word_pos })
    }
}

/// Shift directions used in the LFSR data structure
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
pub enum Direction {
    ShiftLeft,
    ShiftRight,
}

/// Represents a function call in the cipher in the form of function(inputs) = outputs
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
pub struct Transition {
    pub inputs: Vec<StateNodeUID>,
    pub function: FunctionNodeUID,
    pub outputs: Vec<StateNodeUID>,
}

/// A function node UID
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub struct FunctionNodeUID {
    pub uid: usize,
}

impl Serialize for FunctionNodeUID {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        self.uid.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for FunctionNodeUID {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        Deserialize::deserialize(deserializer)
            .map(|uid: usize| FunctionNodeUID { uid })
    }
}

/// A function node UID
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub struct StateNodeUID {
    pub uid: usize,
}

impl Serialize for StateNodeUID {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        self.uid.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for StateNodeUID {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        Deserialize::deserialize(deserializer)
            .map(|uid: usize| StateNodeUID { uid })
    }
}

#[cfg(test)]
mod tests {
    use std::collections::BTreeSet;
    use std::io;
    use serde_json::ser::to_string;
    use crate::common::Domain;
    use crate::common::tests::{read_json_from, read_text_from};
    use crate::specification::{BitAndWord, Direction, Function, FunctionNode, FunctionNodeUID, LinearFunction, StateNode, StateNodeUID, Transition};
    use crate::specification::Direction::{ShiftLeft, ShiftRight};
    use crate::specification::LinearFunction::{AndConst, CircularShift, Concat, GFMul, LFSR, OrConst, Shift, Split};

    #[test]
    fn serialize_variable() -> io::Result<()> {
        let variable = StateNode::Variable {
            domain: Domain::Range { min: 0, max: 256 }
        };
        assert_eq!(read_text_from("state_node/variable")?, to_string(&variable)?);
        Ok(())
    }

    #[test]
    fn deserialize_variable() -> io::Result<()> {
        let variable = StateNode::Variable {
            domain: Domain::Range { min: 0, max: 256 },
        };
        assert_eq!(variable, read_json_from("state_node/variable")?);
        Ok(())
    }

    #[test]
    fn serialize_constant() -> io::Result<()> {
        let mut values = BTreeSet::new();
        values.insert(10);
        let constant = StateNode::Constant {
            value: 10,
        };
        assert_eq!(read_text_from("state_node/constant")?, to_string(&constant)?);
        Ok(())
    }

    #[test]
    fn deserialize_constant() -> io::Result<()> {
        let constant = StateNode::Constant {
            value: 10,
        };
        assert_eq!(constant, read_json_from("state_node/constant")?);
        Ok(())
    }

    #[test]
    fn serialize_function_node() -> io::Result<()> {
        let byte = Domain::Range { min: 0, max: 256 };
        let function_node = FunctionNode {
            domain: vec![byte.clone()],
            co_domain: vec![byte.clone()],
            function: Function::Linear { linear: LinearFunction::Equal },
        };
        assert_eq!(read_text_from("function_node")?, to_string(&function_node)?);
        Ok(())
    }

    #[test]
    fn deserialize_function_node() -> io::Result<()> {
        let byte = Domain::Range { min: 0, max: 256 };
        let function_node = FunctionNode {
            domain: vec![byte.clone()],
            co_domain: vec![byte.clone()],
            function: Function::Linear { linear: LinearFunction::Equal },
        };
        assert_eq!(function_node, read_json_from("function_node")?);
        Ok(())
    }

    #[test]
    fn serialize_equal() -> io::Result<()> {
        assert_eq!(read_text_from("function/equal")?, to_string(&LinearFunction::Equal)?);
        Ok(())
    }

    #[test]
    fn deserialize_equal() -> io::Result<()> {
        assert_eq!(LinearFunction::Equal, read_json_from("function/equal")?);
        Ok(())
    }

    #[test]
    fn serialize_bitwise_xor() -> io::Result<()> {
        assert_eq!(read_text_from("function/bitwise_xor")?, to_string(&LinearFunction::BitwiseXOR)?);
        Ok(())
    }

    #[test]
    fn deserialize_bitwise_xor() -> io::Result<()> {
        assert_eq!(LinearFunction::BitwiseXOR, read_json_from("function/bitwise_xor")?);
        Ok(())
    }

    #[test]
    fn serialize_sbox() -> io::Result<()> {
        let aes_sbox = Function::SBox {
            lookup_table: vec![
                0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
                0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
                0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
                0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
                0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
                0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
                0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
                0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
                0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
                0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
                0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
                0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
                0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
                0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
                0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
                0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16,
            ]
        };
        assert_eq!(read_text_from("function/sbox")?, to_string(&aes_sbox)?);
        Ok(())
    }

    #[test]
    fn deserialize_sbox() -> io::Result<()> {
        let aes_sbox = Function::SBox {
            lookup_table: vec![
                0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
                0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
                0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
                0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
                0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
                0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
                0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
                0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
                0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
                0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
                0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
                0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
                0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
                0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
                0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
                0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16,
            ]
        };
        assert_eq!(aes_sbox, read_json_from("function/sbox")?);
        Ok(())
    }

    #[test]
    fn serialize_table() -> io::Result<()> {
        let nibble_xor = LinearFunction::Table {
            tuples: vec![
                vec![0, 0, 0],
                vec![0, 1, 1],
                vec![0, 2, 2],
                vec![0, 3, 3],
                vec![0, 4, 4],
                vec![0, 5, 5],
                vec![0, 6, 6],
                vec![0, 7, 7],
                vec![0, 8, 8],
                vec![0, 9, 9],
                vec![0, 10, 10],
                vec![0, 11, 11],
                vec![0, 12, 12],
                vec![0, 13, 13],
                vec![0, 14, 14],
                vec![0, 15, 15],
                vec![1, 0, 1],
                vec![1, 1, 0],
                vec![1, 2, 3],
                vec![1, 3, 2],
                vec![1, 4, 5],
                vec![1, 5, 4],
                vec![1, 6, 7],
                vec![1, 7, 6],
                vec![1, 8, 9],
                vec![1, 9, 8],
                vec![1, 10, 11],
                vec![1, 11, 10],
                vec![1, 12, 13],
                vec![1, 13, 12],
                vec![1, 14, 15],
                vec![1, 15, 14],
                vec![2, 0, 2],
                vec![2, 1, 3],
                vec![2, 2, 0],
                vec![2, 3, 1],
                vec![2, 4, 6],
                vec![2, 5, 7],
                vec![2, 6, 4],
                vec![2, 7, 5],
                vec![2, 8, 10],
                vec![2, 9, 11],
                vec![2, 10, 8],
                vec![2, 11, 9],
                vec![2, 12, 14],
                vec![2, 13, 15],
                vec![2, 14, 12],
                vec![2, 15, 13],
                vec![3, 0, 3],
                vec![3, 1, 2],
                vec![3, 2, 1],
                vec![3, 3, 0],
                vec![3, 4, 7],
                vec![3, 5, 6],
                vec![3, 6, 5],
                vec![3, 7, 4],
                vec![3, 8, 11],
                vec![3, 9, 10],
                vec![3, 10, 9],
                vec![3, 11, 8],
                vec![3, 12, 15],
                vec![3, 13, 14],
                vec![3, 14, 13],
                vec![3, 15, 12],
                vec![4, 0, 4],
                vec![4, 1, 5],
                vec![4, 2, 6],
                vec![4, 3, 7],
                vec![4, 4, 0],
                vec![4, 5, 1],
                vec![4, 6, 2],
                vec![4, 7, 3],
                vec![4, 8, 12],
                vec![4, 9, 13],
                vec![4, 10, 14],
                vec![4, 11, 15],
                vec![4, 12, 8],
                vec![4, 13, 9],
                vec![4, 14, 10],
                vec![4, 15, 11],
                vec![5, 0, 5],
                vec![5, 1, 4],
                vec![5, 2, 7],
                vec![5, 3, 6],
                vec![5, 4, 1],
                vec![5, 5, 0],
                vec![5, 6, 3],
                vec![5, 7, 2],
                vec![5, 8, 13],
                vec![5, 9, 12],
                vec![5, 10, 15],
                vec![5, 11, 14],
                vec![5, 12, 9],
                vec![5, 13, 8],
                vec![5, 14, 11],
                vec![5, 15, 10],
                vec![6, 0, 6],
                vec![6, 1, 7],
                vec![6, 2, 4],
                vec![6, 3, 5],
                vec![6, 4, 2],
                vec![6, 5, 3],
                vec![6, 6, 0],
                vec![6, 7, 1],
                vec![6, 8, 14],
                vec![6, 9, 15],
                vec![6, 10, 12],
                vec![6, 11, 13],
                vec![6, 12, 10],
                vec![6, 13, 11],
                vec![6, 14, 8],
                vec![6, 15, 9],
                vec![7, 0, 7],
                vec![7, 1, 6],
                vec![7, 2, 5],
                vec![7, 3, 4],
                vec![7, 4, 3],
                vec![7, 5, 2],
                vec![7, 6, 1],
                vec![7, 7, 0],
                vec![7, 8, 15],
                vec![7, 9, 14],
                vec![7, 10, 13],
                vec![7, 11, 12],
                vec![7, 12, 11],
                vec![7, 13, 10],
                vec![7, 14, 9],
                vec![7, 15, 8],
                vec![8, 0, 8],
                vec![8, 1, 9],
                vec![8, 2, 10],
                vec![8, 3, 11],
                vec![8, 4, 12],
                vec![8, 5, 13],
                vec![8, 6, 14],
                vec![8, 7, 15],
                vec![8, 8, 0],
                vec![8, 9, 1],
                vec![8, 10, 2],
                vec![8, 11, 3],
                vec![8, 12, 4],
                vec![8, 13, 5],
                vec![8, 14, 6],
                vec![8, 15, 7],
                vec![9, 0, 9],
                vec![9, 1, 8],
                vec![9, 2, 11],
                vec![9, 3, 10],
                vec![9, 4, 13],
                vec![9, 5, 12],
                vec![9, 6, 15],
                vec![9, 7, 14],
                vec![9, 8, 1],
                vec![9, 9, 0],
                vec![9, 10, 3],
                vec![9, 11, 2],
                vec![9, 12, 5],
                vec![9, 13, 4],
                vec![9, 14, 7],
                vec![9, 15, 6],
                vec![10, 0, 10],
                vec![10, 1, 11],
                vec![10, 2, 8],
                vec![10, 3, 9],
                vec![10, 4, 14],
                vec![10, 5, 15],
                vec![10, 6, 12],
                vec![10, 7, 13],
                vec![10, 8, 2],
                vec![10, 9, 3],
                vec![10, 10, 0],
                vec![10, 11, 1],
                vec![10, 12, 6],
                vec![10, 13, 7],
                vec![10, 14, 4],
                vec![10, 15, 5],
                vec![11, 0, 11],
                vec![11, 1, 10],
                vec![11, 2, 9],
                vec![11, 3, 8],
                vec![11, 4, 15],
                vec![11, 5, 14],
                vec![11, 6, 13],
                vec![11, 7, 12],
                vec![11, 8, 3],
                vec![11, 9, 2],
                vec![11, 10, 1],
                vec![11, 11, 0],
                vec![11, 12, 7],
                vec![11, 13, 6],
                vec![11, 14, 5],
                vec![11, 15, 4],
                vec![12, 0, 12],
                vec![12, 1, 13],
                vec![12, 2, 14],
                vec![12, 3, 15],
                vec![12, 4, 8],
                vec![12, 5, 9],
                vec![12, 6, 10],
                vec![12, 7, 11],
                vec![12, 8, 4],
                vec![12, 9, 5],
                vec![12, 10, 6],
                vec![12, 11, 7],
                vec![12, 12, 0],
                vec![12, 13, 1],
                vec![12, 14, 2],
                vec![12, 15, 3],
                vec![13, 0, 13],
                vec![13, 1, 12],
                vec![13, 2, 15],
                vec![13, 3, 14],
                vec![13, 4, 9],
                vec![13, 5, 8],
                vec![13, 6, 11],
                vec![13, 7, 10],
                vec![13, 8, 5],
                vec![13, 9, 4],
                vec![13, 10, 7],
                vec![13, 11, 6],
                vec![13, 12, 1],
                vec![13, 13, 0],
                vec![13, 14, 3],
                vec![13, 15, 2],
                vec![14, 0, 14],
                vec![14, 1, 15],
                vec![14, 2, 12],
                vec![14, 3, 13],
                vec![14, 4, 10],
                vec![14, 5, 11],
                vec![14, 6, 8],
                vec![14, 7, 9],
                vec![14, 8, 6],
                vec![14, 9, 7],
                vec![14, 10, 4],
                vec![14, 11, 5],
                vec![14, 12, 2],
                vec![14, 13, 3],
                vec![14, 14, 0],
                vec![14, 15, 1],
                vec![15, 0, 15],
                vec![15, 1, 14],
                vec![15, 2, 13],
                vec![15, 3, 12],
                vec![15, 4, 11],
                vec![15, 5, 10],
                vec![15, 6, 9],
                vec![15, 7, 8],
                vec![15, 8, 7],
                vec![15, 9, 6],
                vec![15, 10, 5],
                vec![15, 11, 4],
                vec![15, 12, 3],
                vec![15, 13, 2],
                vec![15, 14, 1],
                vec![15, 15, 0],
            ]
        };
        assert_eq!(read_text_from("function/table")?, to_string(&nibble_xor)?);
        Ok(())
    }


    #[test]
    fn deserialize_table() -> io::Result<()> {
        let nibble_xor = LinearFunction::Table {
            tuples: vec![
                vec![0, 0, 0],
                vec![0, 1, 1],
                vec![0, 2, 2],
                vec![0, 3, 3],
                vec![0, 4, 4],
                vec![0, 5, 5],
                vec![0, 6, 6],
                vec![0, 7, 7],
                vec![0, 8, 8],
                vec![0, 9, 9],
                vec![0, 10, 10],
                vec![0, 11, 11],
                vec![0, 12, 12],
                vec![0, 13, 13],
                vec![0, 14, 14],
                vec![0, 15, 15],
                vec![1, 0, 1],
                vec![1, 1, 0],
                vec![1, 2, 3],
                vec![1, 3, 2],
                vec![1, 4, 5],
                vec![1, 5, 4],
                vec![1, 6, 7],
                vec![1, 7, 6],
                vec![1, 8, 9],
                vec![1, 9, 8],
                vec![1, 10, 11],
                vec![1, 11, 10],
                vec![1, 12, 13],
                vec![1, 13, 12],
                vec![1, 14, 15],
                vec![1, 15, 14],
                vec![2, 0, 2],
                vec![2, 1, 3],
                vec![2, 2, 0],
                vec![2, 3, 1],
                vec![2, 4, 6],
                vec![2, 5, 7],
                vec![2, 6, 4],
                vec![2, 7, 5],
                vec![2, 8, 10],
                vec![2, 9, 11],
                vec![2, 10, 8],
                vec![2, 11, 9],
                vec![2, 12, 14],
                vec![2, 13, 15],
                vec![2, 14, 12],
                vec![2, 15, 13],
                vec![3, 0, 3],
                vec![3, 1, 2],
                vec![3, 2, 1],
                vec![3, 3, 0],
                vec![3, 4, 7],
                vec![3, 5, 6],
                vec![3, 6, 5],
                vec![3, 7, 4],
                vec![3, 8, 11],
                vec![3, 9, 10],
                vec![3, 10, 9],
                vec![3, 11, 8],
                vec![3, 12, 15],
                vec![3, 13, 14],
                vec![3, 14, 13],
                vec![3, 15, 12],
                vec![4, 0, 4],
                vec![4, 1, 5],
                vec![4, 2, 6],
                vec![4, 3, 7],
                vec![4, 4, 0],
                vec![4, 5, 1],
                vec![4, 6, 2],
                vec![4, 7, 3],
                vec![4, 8, 12],
                vec![4, 9, 13],
                vec![4, 10, 14],
                vec![4, 11, 15],
                vec![4, 12, 8],
                vec![4, 13, 9],
                vec![4, 14, 10],
                vec![4, 15, 11],
                vec![5, 0, 5],
                vec![5, 1, 4],
                vec![5, 2, 7],
                vec![5, 3, 6],
                vec![5, 4, 1],
                vec![5, 5, 0],
                vec![5, 6, 3],
                vec![5, 7, 2],
                vec![5, 8, 13],
                vec![5, 9, 12],
                vec![5, 10, 15],
                vec![5, 11, 14],
                vec![5, 12, 9],
                vec![5, 13, 8],
                vec![5, 14, 11],
                vec![5, 15, 10],
                vec![6, 0, 6],
                vec![6, 1, 7],
                vec![6, 2, 4],
                vec![6, 3, 5],
                vec![6, 4, 2],
                vec![6, 5, 3],
                vec![6, 6, 0],
                vec![6, 7, 1],
                vec![6, 8, 14],
                vec![6, 9, 15],
                vec![6, 10, 12],
                vec![6, 11, 13],
                vec![6, 12, 10],
                vec![6, 13, 11],
                vec![6, 14, 8],
                vec![6, 15, 9],
                vec![7, 0, 7],
                vec![7, 1, 6],
                vec![7, 2, 5],
                vec![7, 3, 4],
                vec![7, 4, 3],
                vec![7, 5, 2],
                vec![7, 6, 1],
                vec![7, 7, 0],
                vec![7, 8, 15],
                vec![7, 9, 14],
                vec![7, 10, 13],
                vec![7, 11, 12],
                vec![7, 12, 11],
                vec![7, 13, 10],
                vec![7, 14, 9],
                vec![7, 15, 8],
                vec![8, 0, 8],
                vec![8, 1, 9],
                vec![8, 2, 10],
                vec![8, 3, 11],
                vec![8, 4, 12],
                vec![8, 5, 13],
                vec![8, 6, 14],
                vec![8, 7, 15],
                vec![8, 8, 0],
                vec![8, 9, 1],
                vec![8, 10, 2],
                vec![8, 11, 3],
                vec![8, 12, 4],
                vec![8, 13, 5],
                vec![8, 14, 6],
                vec![8, 15, 7],
                vec![9, 0, 9],
                vec![9, 1, 8],
                vec![9, 2, 11],
                vec![9, 3, 10],
                vec![9, 4, 13],
                vec![9, 5, 12],
                vec![9, 6, 15],
                vec![9, 7, 14],
                vec![9, 8, 1],
                vec![9, 9, 0],
                vec![9, 10, 3],
                vec![9, 11, 2],
                vec![9, 12, 5],
                vec![9, 13, 4],
                vec![9, 14, 7],
                vec![9, 15, 6],
                vec![10, 0, 10],
                vec![10, 1, 11],
                vec![10, 2, 8],
                vec![10, 3, 9],
                vec![10, 4, 14],
                vec![10, 5, 15],
                vec![10, 6, 12],
                vec![10, 7, 13],
                vec![10, 8, 2],
                vec![10, 9, 3],
                vec![10, 10, 0],
                vec![10, 11, 1],
                vec![10, 12, 6],
                vec![10, 13, 7],
                vec![10, 14, 4],
                vec![10, 15, 5],
                vec![11, 0, 11],
                vec![11, 1, 10],
                vec![11, 2, 9],
                vec![11, 3, 8],
                vec![11, 4, 15],
                vec![11, 5, 14],
                vec![11, 6, 13],
                vec![11, 7, 12],
                vec![11, 8, 3],
                vec![11, 9, 2],
                vec![11, 10, 1],
                vec![11, 11, 0],
                vec![11, 12, 7],
                vec![11, 13, 6],
                vec![11, 14, 5],
                vec![11, 15, 4],
                vec![12, 0, 12],
                vec![12, 1, 13],
                vec![12, 2, 14],
                vec![12, 3, 15],
                vec![12, 4, 8],
                vec![12, 5, 9],
                vec![12, 6, 10],
                vec![12, 7, 11],
                vec![12, 8, 4],
                vec![12, 9, 5],
                vec![12, 10, 6],
                vec![12, 11, 7],
                vec![12, 12, 0],
                vec![12, 13, 1],
                vec![12, 14, 2],
                vec![12, 15, 3],
                vec![13, 0, 13],
                vec![13, 1, 12],
                vec![13, 2, 15],
                vec![13, 3, 14],
                vec![13, 4, 9],
                vec![13, 5, 8],
                vec![13, 6, 11],
                vec![13, 7, 10],
                vec![13, 8, 5],
                vec![13, 9, 4],
                vec![13, 10, 7],
                vec![13, 11, 6],
                vec![13, 12, 1],
                vec![13, 13, 0],
                vec![13, 14, 3],
                vec![13, 15, 2],
                vec![14, 0, 14],
                vec![14, 1, 15],
                vec![14, 2, 12],
                vec![14, 3, 13],
                vec![14, 4, 10],
                vec![14, 5, 11],
                vec![14, 6, 8],
                vec![14, 7, 9],
                vec![14, 8, 6],
                vec![14, 9, 7],
                vec![14, 10, 4],
                vec![14, 11, 5],
                vec![14, 12, 2],
                vec![14, 13, 3],
                vec![14, 14, 0],
                vec![14, 15, 1],
                vec![15, 0, 15],
                vec![15, 1, 14],
                vec![15, 2, 13],
                vec![15, 3, 12],
                vec![15, 4, 11],
                vec![15, 5, 10],
                vec![15, 6, 9],
                vec![15, 7, 8],
                vec![15, 8, 7],
                vec![15, 9, 6],
                vec![15, 10, 5],
                vec![15, 11, 4],
                vec![15, 12, 3],
                vec![15, 13, 2],
                vec![15, 14, 1],
                vec![15, 15, 0],
            ]
        };
        assert_eq!(nibble_xor, read_json_from("function/table")?);
        Ok(())
    }

    #[test]
    fn serialize_command() -> io::Result<()> {
        let user_command = LinearFunction::Command {
            executable: String::from("user_command")
        };
        assert_eq!(read_text_from("function/command")?, to_string(&user_command)?);
        Ok(())
    }

    #[test]
    fn deserialize_command() -> io::Result<()> {
        let user_command = LinearFunction::Command {
            executable: String::from("user_command")
        };
        assert_eq!(user_command, read_json_from("function/command")?);
        Ok(())
    }

    #[test]
    fn serialize_gf_mul() -> io::Result<()> {
        let aes_poly = GFMul { poly: 0x11B, constant: 0x53 };
        assert_eq!(read_text_from("function/gfmul")?, to_string(&aes_poly)?);
        Ok(())
    }

    #[test]
    fn deserialize_gf_mul() -> io::Result<()> {
        let aes_poly = GFMul { poly: 0x11B, constant: 0x53 };
        assert_eq!(aes_poly, read_json_from("function/gfmul")?);
        Ok(())
    }

    #[test]
    fn serialize_gf_mat_to_vec_mul() -> io::Result<()> {
        let aes_mat_mul = LinearFunction::GFMatToVecMul {
            poly: 0x11B,
            m: vec![
                vec![2, 3, 1, 1],
                vec![1, 2, 3, 1],
                vec![1, 1, 2, 3],
                vec![3, 1, 1, 2]],
        };
        assert_eq!(read_text_from("function/gfmattovecmul")?, to_string(&aes_mat_mul)?);
        Ok(())
    }

    #[test]
    fn deserialize_gf_mat_to_vec_mul() -> io::Result<()> {
        let aes_mat_mul = LinearFunction::GFMatToVecMul {
            poly: 0x11B,
            m: vec![
                vec![2, 3, 1, 1],
                vec![1, 2, 3, 1],
                vec![1, 1, 2, 3],
                vec![3, 1, 1, 2]],
        };
        assert_eq!(aes_mat_mul, read_json_from("function/gfmattovecmul")?);
        Ok(())
    }

    #[test]
    fn serialize_lfsr() -> io::Result<()> {
        let lfsr = LFSR { nb_bits: 8, poly: 0b0101, direction: Direction::ShiftLeft };
        assert_eq!(read_text_from("function/lfsr")?, to_string(&lfsr)?);
        Ok(())
    }

    #[test]
    fn deserialize_lfsr() -> io::Result<()> {
        let lfsr = LFSR { nb_bits: 8, poly: 0b0101, direction: Direction::ShiftLeft };
        assert_eq!(lfsr, read_json_from("function/lfsr")?);
        Ok(())
    }

    #[test]
    fn serialize_split() -> io::Result<()> {
        let split = Split {
            to_words: vec![
                vec![7, 6, 5, 4],
                vec![3, 2, 1, 0],
            ]
        };
        assert_eq!(read_text_from("function/split")?, to_string(&split)?);
        Ok(())
    }

    #[test]
    fn deserialize_split() -> io::Result<()> {
        let split = Split {
            to_words: vec![
                vec![7, 6, 5, 4],
                vec![3, 2, 1, 0],
            ]
        };
        assert_eq!(split, read_json_from("function/split")?);
        Ok(())
    }

    #[test]
    fn serialize_concat() -> io::Result<()> {
        let concat = Concat {
            to_word: vec![
                BitAndWord { bit_pos: 1, word_pos: 1 },
                BitAndWord { bit_pos: 0, word_pos: 1 },
                BitAndWord { bit_pos: 1, word_pos: 0 },
                BitAndWord { bit_pos: 0, word_pos: 0 },
            ]
        };
        assert_eq!(read_text_from("function/concat")?, to_string(&concat)?);
        Ok(())
    }

    #[test]
    fn deserialize_concat() -> io::Result<()> {
        let concat = Concat {
            to_word: vec![
                BitAndWord { bit_pos: 1, word_pos: 1 },
                BitAndWord { bit_pos: 0, word_pos: 1 },
                BitAndWord { bit_pos: 1, word_pos: 0 },
                BitAndWord { bit_pos: 0, word_pos: 0 },
            ]
        };
        assert_eq!(concat, read_json_from("function/concat")?);
        Ok(())
    }

    #[test]
    fn serialize_shift() -> io::Result<()> {
        let shift = Shift {
            nb_bits: 8,
            bit_count: 3,
            direction: ShiftRight
        };
        assert_eq!(read_text_from("function/shift")?, to_string(&shift)?);
        Ok(())
    }

    #[test]
    fn deserialize_shift() -> io::Result<()> {
        let shift = Shift {
            nb_bits: 8,
            bit_count: 3,
            direction: ShiftRight
        };
        assert_eq!(shift, read_json_from("function/shift")?);
        Ok(())
    }

    #[test]
    fn serialize_circular_shift() -> io::Result<()> {
        let shift = CircularShift {
            nb_bits: 8,
            bit_count: 6,
            direction: ShiftLeft
        };
        assert_eq!(read_text_from("function/circular_shift")?, to_string(&shift)?);
        Ok(())
    }

    #[test]
    fn deserialize_circular_shift() -> io::Result<()> {
        let shift = CircularShift {
            nb_bits: 8,
            bit_count: 6,
            direction: ShiftLeft
        };
        assert_eq!(shift, read_json_from("function/circular_shift")?);
        Ok(())
    }

    #[test]
    fn serialize_and_const() -> io::Result<()> {
        let and_const = AndConst {
            constant: 83
        };
        assert_eq!(read_text_from("function/and_const")?, to_string(&and_const)?);
        Ok(())
    }

    #[test]
    fn deserialize_and_const() -> io::Result<()> {
        let and_const = AndConst {
            constant: 83
        };
        assert_eq!(and_const, read_json_from("function/and_const")?);
        Ok(())
    }

    #[test]
    fn serialize_or_const() -> io::Result<()> {
        let or_const = OrConst {
            constant: 83
        };
        assert_eq!(read_text_from("function/or_const")?, to_string(&or_const)?);
        Ok(())
    }

    #[test]
    fn deserialize_or_const() -> io::Result<()> {
        let or_const = OrConst {
            constant: 83
        };
        assert_eq!(or_const, read_json_from("function/or_const")?);
        Ok(())
    }

    #[test]
    fn serialize_bit_and_word() -> io::Result<()> {
        let bit_and_word = BitAndWord { bit_pos: 1, word_pos: 2 };
        assert_eq!(read_text_from("bit_and_word")?, to_string(&bit_and_word)?);
        Ok(())
    }

    #[test]
    fn deserialize_bit_and_word() -> io::Result<()> {
        let bit_and_word = BitAndWord { bit_pos: 1, word_pos: 2 };
        assert_eq!(bit_and_word, read_json_from("bit_and_word")?);
        Ok(())
    }

    #[test]
    fn serialize_shiftleft() -> io::Result<()> {
        assert_eq!(read_text_from("shiftleft")?, to_string(&Direction::ShiftLeft)?);
        Ok(())
    }

    #[test]
    fn deserialize_shiftleft() -> io::Result<()> {
        assert_eq!(Direction::ShiftLeft, read_json_from("shiftleft")?);
        Ok(())
    }

    #[test]
    fn serialize_shiftright() -> io::Result<()> {
        assert_eq!(read_text_from("shiftright")?, to_string(&Direction::ShiftRight)?);
        Ok(())
    }

    #[test]
    fn deserialize_shiftright() -> io::Result<()> {
        assert_eq!(Direction::ShiftRight, read_json_from("shiftright")?);
        Ok(())
    }

    #[test]
    fn serialize_transition() -> io::Result<()> {
        let transition = Transition {
            inputs: vec![StateNodeUID { uid: 0 }],
            function: FunctionNodeUID { uid: 0 },
            outputs: vec![StateNodeUID { uid: 1 }],
        };
        assert_eq!(read_text_from("transition")?, to_string(&transition)?);
        Ok(())
    }

    #[test]
    fn deserialize_transition() -> io::Result<()> {
        let transition = Transition {
            inputs: vec![StateNodeUID { uid: 0 }],
            function: FunctionNodeUID { uid: 0 },
            outputs: vec![StateNodeUID { uid: 1 }],
        };
        assert_eq!(transition, read_json_from("transition")?);
        Ok(())
    }

    #[test]
    fn serialize_function_node_uid() -> io::Result<()> {
        let function_node_uid = FunctionNodeUID { uid: 10 };
        assert_eq!(read_text_from("function_node_uid")?, to_string(&function_node_uid)?);
        Ok(())
    }

    #[test]
    fn deserialize_function_node_uid() -> io::Result<()> {
        let function_node_uid = FunctionNodeUID { uid: 10 };
        assert_eq!(function_node_uid, read_json_from("function_node_uid")?);
        Ok(())
    }

    #[test]
    fn serialize_state_node_uid() -> io::Result<()> {
        let state_node_uid = StateNodeUID { uid: 4 };
        assert_eq!(read_text_from("state_node_uid")?, to_string(&state_node_uid)?);
        Ok(())
    }

    #[test]
    fn deserialize_state_node_uid() -> io::Result<()> {
        let state_node_uid = StateNodeUID { uid: 4 };
        assert_eq!(state_node_uid, read_json_from("state_node_uid")?);
        Ok(())
    }
}