use crate::common::Value;
use crate::specification::{BitAndWord, Direction};

pub fn equal(v: Value) -> Vec<Value> { vec![v] }

pub fn equal_into(v: Value, output: &mut Vec<Value>) { output[0] = v; }

pub fn bitwise_xor(args: &Vec<Value>) -> Vec<Value> {
    vec![args.iter().fold(0, |acc, v| acc ^ v)]
}

pub fn bitwise_xor_into(args: &Vec<Value>, output: &mut Vec<Value>) {
    output[0] = args.iter().fold(0, |acc, v| acc ^ v);
}

pub fn sbox(lookup_table: &Vec<Value>, v: Value) -> Vec<Value> {
    vec![lookup_table[v as usize]]
}

pub fn sbox_into(lookup_table: &Vec<Value>, v: Value, output: &mut Vec<Value>) {
    output[0] = lookup_table[v as usize];
}

pub fn table(tuples: &Vec<Vec<Value>>, args: &Vec<Value>) -> Vec<Value> {
    let mut solution = None;
    for tuple in tuples {
        if args.as_slice() == &tuple[0..args.len()] {
            if solution.is_none() {
                solution = Some(Vec::from(&tuple[args.len()..tuple.len()]));
            } else {
                panic!("duplicate entries for {:?}", args);
            }
        }
    }
    if let Some(output) = solution {
        output
    } else {
        panic!("no entry found for {:?}", args)
    }
}

pub fn table_into(tuples: &Vec<Vec<Value>>, args: &Vec<Value>, output: &mut Vec<Value>) {
    let mut solution = None;
    for tuple in tuples {
        if args.as_slice() == &tuple[0..args.len()] {
            if solution.is_none() {
                solution = Some(Vec::from(&tuple[args.len()..tuple.len()]));
            } else {
                panic!("duplicate entries for {:?}", args);
            }
        }
    }
    if let Some(solution) = solution {
        solution.iter().enumerate().for_each(|(i, value)| output[i] = *value);
    } else {
        panic!("no entry found for {:?}", args)
    }
}

pub fn gf_mul(poly: usize, mut a: Value, mut b: Value) -> Vec<Value> {
    let nb_bits = usize::BITS - 1 - poly.leading_zeros();
    let high_bit = 1 << (nb_bits - 1);

    let mut product = 0;

    for _ in 0..nb_bits {
        if (b & 1) != 0 {
            product ^= a;
        }
        let high_bit_set = (a & high_bit) != 0;
        a <<= 1;
        if high_bit_set {
            a ^= poly;
        }
        b >>= 1;
    }
    vec![product]
}

pub fn gf_mul_into(poly: usize, mut a: Value, mut b: Value, output: &mut Vec<Value>) {
    let nb_bits = usize::BITS - 1 - poly.leading_zeros();
    let high_bit = 1 << (nb_bits - 1);

    let mut product = 0;

    for _ in 0..nb_bits {
        if (b & 1) != 0 {
            product ^= a;
        }
        let high_bit_set = (a & high_bit) != 0;
        a <<= 1;
        if high_bit_set {
            a ^= poly;
        }
        b >>= 1;
    }
    output[0] = product
}

pub fn gf_mat_to_vec_mul(poly: Value, m: &Vec<Vec<Value>>, args: &Vec<Value>) -> Vec<Value> {
    let mut result = vec![0; args.len()];

    for i in 0..m.len() {
        for j in 0..m[i].len() {
            result[i] ^= gf_mul(poly, m[i][j], args[j])[0];
        }
    }

    result
}

pub fn gf_mat_to_vec_mul_into(poly: Value, m: &Vec<Vec<Value>>, args: &Vec<Value>, output: &mut Vec<Value>) {
    for i in 0..args.len() {
        output[i] = 0;
    }
    for i in 0..m.len() {
        for j in 0..m[i].len() {
            output[i] ^= gf_mul(poly, m[i][j], args[j])[0];
        }
    }
}

pub fn lfsr(nb_bits: usize, poly: usize, direction: &Direction, value: Value) -> Vec<Value> {
    let word_mask = (1 << nb_bits) - 1;
    let mut inserted_bits = 0;
    for i in 0..nb_bits {
        if (value & poly) & (1 << i) != 0 {
            inserted_bits ^= 1;
        }
    }

    match direction {
        &Direction::ShiftLeft => vec![((value << 1) | inserted_bits) & word_mask],
        &Direction::ShiftRight => vec![(inserted_bits << nb_bits - 1) | (value >> 1)]
    }
}

pub fn lfsr_into(nb_bits: usize, poly: usize, direction: &Direction, value: Value, output: &mut Vec<Value>) {
    let word_mask = (1 << nb_bits) - 1;
    let mut inserted_bits = 0;
    for i in 0..nb_bits {
        if (value & poly) & (1 << i) != 0 {
            inserted_bits ^= 1;
        }
    }

    match direction {
        &Direction::ShiftLeft => output[0] = ((value << 1) | inserted_bits) & word_mask,
        &Direction::ShiftRight => output[0] = (inserted_bits << nb_bits - 1) | (value >> 1)
    }
}

pub fn split(to_words: &Vec<Vec<usize>>, word: Value) -> Vec<Value> {
    let mut words = vec![0; to_words.len()];
    for (i, word_bits) in to_words.iter().enumerate() {
        for bit in word_bits {
            words[i] <<= 1;
            words[i] += if word & (1 << bit) != 0 { 1 } else { 0 };
        }
    }
    words
}

pub fn split_into(to_words: &Vec<Vec<usize>>, word: Value, output: &mut Vec<Value>) {
    for i in 0..to_words.len() {
        output[i] = 0;
    }
    for (i, word_bits) in to_words.iter().enumerate() {
        for bit in word_bits {
            output[i] <<= 1;
            output[i] += if word & (1 << bit) != 0 { 1 } else { 0 };
        }
    }
}

pub fn concat(to_word: &Vec<BitAndWord>, words: &Vec<Value>) -> Vec<Value> {
    let mut word = 0;
    for &BitAndWord { bit_pos, word_pos } in to_word {
        word <<= 1;
        word += if words[word_pos] & (1 << bit_pos) != 0 { 1 } else { 0 }
    }
    vec![word]
}

pub fn concat_into(to_word: &Vec<BitAndWord>, words: &Vec<Value>, output: &mut Vec<Value>) {
    let mut word = 0;
    for &BitAndWord { bit_pos, word_pos } in to_word {
        word <<= 1;
        word += if words[word_pos] & (1 << bit_pos) != 0 { 1 } else { 0 }
    }
    output[0] = word;
}

pub fn shift(nb_bits: usize, bit_count: usize, direction: &Direction, word: Value) -> Vec<Value> {
    let word_mask = (1 << nb_bits) - 1;
    match direction {
        Direction::ShiftLeft => vec![(word << bit_count) & word_mask],
        Direction::ShiftRight => vec![(word >> bit_count) & word_mask]
    }
}

pub fn shift_into(nb_bits: usize, bit_count: usize, direction: &Direction, word: Value, output: &mut Vec<Value>) {
    let word_mask = (1 << nb_bits) - 1;
    match direction {
        Direction::ShiftLeft => output[0] = (word << bit_count) & word_mask,
        Direction::ShiftRight => output[0] = (word >> bit_count) & word_mask
    }
}

pub fn circular_shift(nb_bits: usize, bit_count: usize, direction: &Direction, word: Value) -> Vec<Value> {
    let word_mask = (1 << nb_bits) - 1;
    match direction {
        Direction::ShiftLeft => vec![((word << bit_count) | (word >> (nb_bits - bit_count))) & word_mask],
        Direction::ShiftRight => vec![((word >> bit_count) | (word << (nb_bits - bit_count))) & word_mask]
    }
}

pub fn circular_shift_into(nb_bits: usize, bit_count: usize, direction: &Direction, word: Value, output: &mut Vec<Value>) {
    let word_mask = (1 << nb_bits) - 1;
    match direction {
        Direction::ShiftLeft => output[0] = ((word << bit_count) | (word >> (nb_bits - bit_count))) & word_mask,
        Direction::ShiftRight => output[0] = ((word >> bit_count) | (word << (nb_bits - bit_count))) & word_mask
    }
}


pub fn and_const(constant: Value, word: Value) -> Vec<Value> {
    vec![constant & word]
}

pub fn and_const_into(constant: Value, word: Value, output: &mut Vec<Value>) {
    output[0] = constant & word;
}

pub fn or_const(constant: Value, word: Value) -> Vec<Value> {
    vec![constant | word]
}

pub fn or_const_into(constant: Value, word: Value, output: &mut Vec<Value>) {
    output[0] = constant | word;
}
