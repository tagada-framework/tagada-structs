package fr.limos.gitlab.decrypt.tagada.structs

import fr.limos.gitlab.decrypt.tagada.structs.common.Range
import fr.limos.gitlab.decrypt.tagada.structs.common.Sparse
import fr.limos.gitlab.decrypt.tagada.structs.common.Version
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class Common {

    @Test
    fun serializeVersion() {
        val version = Version(1, 2, 3)
        assertEquals(readTextFrom("version"), mapper.writeValueAsString(version))
    }

    @Test
    fun deserializeVersion() {
        val version = Version(1, 2, 3)
        assertEquals(version, readJsonFrom<Version>("version"))
    }


    @Test
    fun serializeRangeDomain() {
        val nibble = Range(0, 16)
        assertEquals(readTextFrom("domain/range"), mapper.writeValueAsString(nibble))
    }

    @Test
    fun deserializeRangeDomain() {
        val nibble = Range(0, 16)
        assertEquals(nibble, readJsonFrom<Range>("domain/range"))
    }

    @Test
    fun serializeSparseDomain() {
        val nibble = Sparse((0 until 16).toSet())
        assertEquals(readTextFrom("domain/sparse"), mapper.writeValueAsString(nibble))
    }

    @Test
    fun deserializeSparseDomain() {
        val nibble = Sparse((0 until 16).toSet())
        assertEquals(nibble, readJsonFrom<Sparse>("domain/sparse"))
    }

}